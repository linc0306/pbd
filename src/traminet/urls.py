from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'traminet.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^home/', include('app_traminet.urls')),
    url(r'^$', 'app_traminet.views.home', name='home'),
    url(r'^tramites/$', 'app_traminet.views.tramites', name='tramites'),
    url(r'^categorias/$', 'app_traminet.views.listado_categorias', name='categorias'),
    url(r'^organismos/$', 'app_traminet.views.listado_organismos', name='organismos'),
    url(r'^documentos/$', 'app_traminet.views.listado_documentos', name='documentos'),
    url(r'^categorias/(\d+)$', 'app_traminet.views.listado_categorias_tramite', name='categorias_n'),
    url(r'^sobre-nosotros/', 'app_traminet.views.nosotros', name='nosotros'),
    url(r'^registro/', 'app_traminet.views.registro', name='registro'),
    url(r'^mas-visitados/', 'app_traminet.views.listado_mas_visitados', name='visitados'),
    url(r'^destacados/', 'app_traminet.views.listado_destacados', name='destacados'),
    url(r'^tramites/(\d+)/$', 'app_traminet.views.mostrar_tramite', name='tramite_n'),
    url(r'^organismos/(\d+)$', 'app_traminet.views.mostrar_organismo', name='organismo_n'),
    url(r'^documentos/(\d+)$', 'app_traminet.views.mostrar_documento', name='documento_n'),
    url(r'^auditor/$', 'app_traminet.views.auditor', name='auditor'),
    url(r'^consultar_auditoria$', 'app_traminet.views.auditor', name='auditor_consulta'),
    url(r'^config_auditoria$', 'app_traminet.views.auditor_configuracion', name='auditor_configuracion'),
    url(r'^limpiar_auditoria$', 'app_traminet.views.auditor_limpiar', name='auditor_limpiar'),
    url(r'^admin_sitio/$', 'app_traminet.views.admin_sitio', name='admin_sitio'),
    #url(r'^tramites/(\d+)#tab(\d+)$', 'app_traminet.views.mostrar_modalidades', name='modalidad_n'),
    url(r'^tramites/(\d+)/(\d+)$', 'app_traminet.views.mostrar_modalidades', name='actividad_n'),   
    url(r'^organismos_ai/$', 'app_traminet.views.organismos_ai', name='organismos_ai'),
    url(r'^agregar_organismo/$', 'app_traminet.views.agregar_organismo', name='agregar_organismo'),
    url(r'^documentos_ai/$', 'app_traminet.views.documentos_ai', name='documentos_ai'),
    url(r'^agregar_documento/$', 'app_traminet.views.agregar_documento', name='agregar_documento'),
    url(r'^agregar_oficina/$', 'app_traminet.views.agregar_oficina', name='agregar_oficina'),
    url(r'^agregar_actividad/$', 'app_traminet.views.agregar_actividad', name='agregar_actividad'),
    url(r'^agregar_ley/$', 'app_traminet.views.agregar_ley', name='agregar_ley'),
    url(r'^agregar_requisito/$', 'app_traminet.views.agregar_requisito', name='agregar_requisito'),
    url(r'^agregar_tramite/$', 'app_traminet.views.agregar_tramite', name='agregar_tramite'),


    # Views login
    url(r'^accounts/login/$', 'app_traminet.views.login'),
    url(r'^accounts/auth/$', 'app_traminet.views.auth_view'),
    url(r'^accounts/logout/$', 'app_traminet.views.logout'),
    url(r'^accounts/loggedin/$', 'app_traminet.views.loggedin'),
    url(r'^accounts/invalid/$', 'app_traminet.views.invalid_login'),

    # Views register
    url(r'^accounts/register/$', 'app_traminet.views.register_user'),
    url(r'^accounts/register_sucess/$', 'app_traminet.views.register_sucess'),

    url(r'^admin/', include(admin.site.urls)),
)

handler404= 'traminet.view.handler404'
handler500 ='traminet.views.handler500'

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL,
                              document_root=settings.STATIC_ROOT)

	urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)
