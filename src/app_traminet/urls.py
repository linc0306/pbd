from django.conf.urls import include, url
from django.contrib import admin

from app_traminet import views

urlpatterns = [
    #url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'admin/', include(admin.site.urls)),
]

handler404= 'traminet.views.handler404'

