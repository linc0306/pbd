# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm



class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    
    # username = forms.RegexField(label=_("Nombre de usuario"), max_length=30,
    # regex=r'^[\w.@+-]+$',
    # # help_text=_("Obligatorio. 30 characters or fewer. Letters, digits and ""@/./+/-/_ only."),
    # error_messages={
    #         'invalid': _("This value may contain only letters, numbers and "
    #                      "@/./+/-/_ characters.")})
    # password1 = forms.CharField(label=_("Contraseña"), widget=forms.PasswordInput)
    # password2 = forms.CharField(label=_("Confirmación contraseña"),widget=forms.PasswordInput,
    # help_text=_("Ingresar la misma clave a ambos campos."))
     
    
    nombre= forms.CharField(max_length=30)
    apellido = forms.CharField(max_length=30)

    class Meta:
        model = User
        fields = ('username', 'nombre', 'apellido', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.mail = self.cleaned_data['email']
        user.first_name = self.cleaned_data['nombre']
        user.last_name = self.cleaned_data['apellido']

        if commit:
            user.save()

        return user



