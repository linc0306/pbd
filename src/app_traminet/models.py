# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one Field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or Field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Actividad(models.Model):
    actividad_id = models.AutoField(primary_key=True)
    actividad_titulo = models.CharField(max_length=100, blank=True)
    actividad_descripcion = models.CharField(max_length=1000, blank=True)

    class Meta:
        managed = False
        db_table = 'actividad'


class ActividadDoc(models.Model):
    actividad = models.ForeignKey(Actividad)
    documento = models.ForeignKey('Documento')
    actividad_doc_ent = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'actividad_doc'


class ActividadModalidad(models.Model):
    actividad = models.ForeignKey(Actividad)
    modalidad = models.ForeignKey('Modalidad')
    mod_act_duracion = models.IntegerField(blank=True, null=True)
    mod_act_costo = models.IntegerField(blank=True, null=True)
    mod_act_pasos = models.CharField(max_length=1500, blank=True)
    mod_act_contacto = models.CharField(max_length=150, blank=True)
    mod_act_nota = models.CharField(max_length=150, blank=True)

    class Meta:
        managed = False
        db_table = 'actividad_modalidad'


class ActividadOficina(models.Model):
    actividad = models.ForeignKey(Actividad)
    oficina = models.ForeignKey('Oficina')

    class Meta:
        managed = False
        db_table = 'actividad_oficina'


class AuthGroup(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'


class AuthPermission(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'


class AuthUser(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'


class AuthUserUserPermissions(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'


class Categoria(models.Model):
    categoria_id = models.AutoField(primary_key=True)
    categoria_titulo = models.CharField(max_length=30, blank=True)
    categoria_descripcion = models.CharField(max_length=200, blank=True)

    class Meta:
        managed = False
        db_table = 'categoria'


class CategoriasTramite(models.Model):
    tramite = models.ForeignKey('Tramite')
    categoria = models.ForeignKey(Categoria)

    class Meta:
        managed = False
        db_table = 'categorias_tramite'


class Comuna(models.Model):
    comuna_id = models.AutoField(primary_key=True)
    provincia = models.ForeignKey('Provincia')
    comuna_nombre = models.CharField(max_length=25, blank=True)

    class Meta:
        managed = False
        db_table = 'comuna'


class DjangoAdminLog(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'


class DjangoMigrations(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Documento(models.Model):
    documento_id = models.AutoField(primary_key=True)
    documento_titulo = models.CharField(max_length=100, blank=True)
    documento_tipo = models.CharField(max_length=25, blank=True)
    documento_descripcion = models.CharField(max_length=500, blank=True)

    class Meta:
        managed = False
        db_table = 'documento'


class DocumentoTramite(models.Model):
    tramite = models.ForeignKey('Tramite')
    documento = models.ForeignKey(Documento)

    class Meta:
        managed = False
        db_table = 'documento_tramite'


class Edicion(models.Model):
    edicion_id = models.AutoField(primary_key=True)
    usuario = models.ForeignKey('Usuario', blank=True, null=True)
    tramite = models.ForeignKey('Tramite', blank=True, null=True)
    edicion_fecha = models.DateField(blank=True, null=True)
    edicion_tipo = models.CharField(max_length=2, blank=True)
    edicion_descripcion = models.CharField(max_length=200, blank=True)

    class Meta:
        managed = False
        db_table = 'edicion'


class Etapa(models.Model):
    actividad = models.ForeignKey(ActividadModalidad, related_name='etapa_actividad')
    modalidad_act_id = models.IntegerField()
    actividad_sig = models.ForeignKey(ActividadModalidad, related_name='etapa_actividad_sgte')
    modalidad_act_sig_id = models.IntegerField()
    tramite = models.ForeignKey('TramiteModalidad')
    modalidad_tra_id = models.IntegerField()
    etapa_nro = models.IntegerField(blank=True, null=True)
    etapa_espera = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'etapa'


class FichaTecnica(models.Model):
    tramite = models.ForeignKey('Tramite', primary_key=True)
    ficha_numero_visitas = models.IntegerField(blank=True, null=True)
    ficha_fecha_creacion = models.DateField(blank=True, null=True)
    ficha_fecha_actualizado = models.DateField(blank=True, null=True)
    ficha_estado = models.CharField(max_length=2, blank=True)
    ficha_comentario_estado = models.CharField(max_length=200, blank=True)
    ficha_destacado = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'ficha_tecnica'


class FormatoDocumento(models.Model):
    formato_doc_id = models.AutoField(primary_key=True)
    documento = models.ForeignKey(Documento)
    formato_doc_nombre = models.CharField(max_length=2, blank=True)
    formato_doc_costo = models.IntegerField(blank=True, null=True)
    formato_doc_archivo = models.CharField(max_length=254, blank=True)

    class Meta:
        managed = False
        db_table = 'formato_documento'


class Ley(models.Model):
    ley_id = models.AutoField(primary_key=True)
    ley_titulo = models.CharField(max_length=50, blank=True)
    ley_descripcion = models.CharField(max_length=200, blank=True)

    class Meta:
        managed = False
        db_table = 'ley'


class LogSql(models.Model):
    log_id = models.AutoField(primary_key=True)
    log_fecha = models.DateField(blank=True, null=True)
    log_tabla = models.CharField(max_length=30, blank=True)
    log_usuario = models.CharField(max_length=100, blank=True)
    log_operacion = models.CharField(max_length=8, blank=True)
    log_antes = models.CharField(max_length=2000, blank=True)
    log_despues = models.CharField(max_length=2000, blank=True)

    class Meta:
        managed = False
        db_table = 'log_sql'


class Marcado(models.Model):
    tramite = models.ForeignKey('Tramite')
    usuario = models.ForeignKey('Usuario')
    marcado_fecha = models.DateField(blank=True, null=True)
    marcado_notificacion = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'marcado'


class MarcoLegalTramite(models.Model):
    tramite = models.ForeignKey('Tramite')
    ley = models.ForeignKey(Ley)

    class Meta:
        managed = False
        db_table = 'marco_legal_tramite'


class Modalidad(models.Model):
    modalidad_id = models.AutoField(primary_key=True)
    modalidad_titulo = models.CharField(max_length=50, blank=True)
    modalidad_descripcion = models.CharField(max_length=100, blank=True)

    class Meta:
        managed = False
        db_table = 'modalidad'


class Notificacion(models.Model):
    notificacion_id = models.AutoField(primary_key=True)
    notificacion_titulo = models.CharField(max_length=150, blank=True)
    notificacion_descripcion = models.CharField(max_length=250, blank=True)

    class Meta:
        managed = False
        db_table = 'notificacion'


class Oficina(models.Model):
    oficina_id = models.AutoField(primary_key=True)
    comuna = models.ForeignKey(Comuna)
    organismo = models.ForeignKey('Organismo')
    oficina_nombre = models.CharField(max_length=100, blank=True)
    oficina_direccion = models.CharField(max_length=150, blank=True)
    oficina_tel_fijo = models.CharField(max_length=25, blank=True)
    oficina_tel_celu = models.CharField(max_length=25, blank=True)
    oficina_horario = models.CharField(max_length=150, blank=True)

    class Meta:
        managed = False
        db_table = 'oficina'


class OrdenTrabajo(models.Model):
    orden_id = models.AutoField(primary_key=True)
    usuario_envia = models.ForeignKey('Usuario', related_name= 'ordentrabajo_usuario_envia')
    usuario_recibe = models.ForeignKey('Usuario', related_name= 'ordentrabajo_usuario_recibe')
    orden_asunto = models.CharField(max_length=100, blank=True)
    orden_descripcion = models.CharField(max_length=1000, blank=True)
    orden_fecha = models.DateTimeField(blank=True, null=True)
    orden_fecha_entrega = models.DateTimeField(blank=True, null=True)
    orden_estado = models.CharField(max_length=2, blank=True)

    class Meta:
        managed = False
        db_table = 'orden_trabajo'


class Organismo(models.Model):
    organismo_id = models.AutoField(primary_key=True)
    organismo_nombre = models.CharField(max_length=100, blank=True)
    organismo_descripcion = models.CharField(max_length=1000, blank=True)
    organismo_tel_fijo = models.CharField(max_length=25, blank=True)
    organismo_tel_celu = models.CharField(max_length=25, blank=True)
    organismo_pagina_web = models.CharField(max_length=50, blank=True)
    organismo_logo = models.CharField(max_length=254, blank=True)

    class Meta:
        managed = False
        db_table = 'organismo'


class PalabraClave(models.Model):
    palabra_id = models.AutoField(primary_key=True)
    palabra_nombre = models.CharField(max_length=50, blank=True)

    class Meta:
        managed = False
        db_table = 'palabra_clave'


class PalabraFichaTramite(models.Model):
    tramite = models.ForeignKey(FichaTecnica)
    palabra = models.ForeignKey(PalabraClave)

    class Meta:
        managed = False
        db_table = 'palabra_ficha_tramite'


class Provincia(models.Model):
    provincia_id = models.AutoField(primary_key=True)
    region = models.ForeignKey('Region')
    provincia_nombre = models.CharField(max_length=30, blank=True)

    class Meta:
        managed = False
        db_table = 'provincia'


class Region(models.Model):
    region_id = models.AutoField(primary_key=True)
    region_nombre = models.CharField(max_length=60, blank=True)

    class Meta:
        managed = False
        db_table = 'region'


class Relacionados(models.Model):
    tramite = models.ForeignKey('Tramite', related_name='tramite_principal')
    tramite_rel = models.ForeignKey('Tramite', related_name='tramite_relacionado')
    relacionados_info = models.CharField(max_length=300, blank=True)


    class Meta:
        managed = False
        db_table = 'relacionados'


class Requisito(models.Model):
    requisito_id = models.AutoField(primary_key=True)
    requisito_titulo = models.CharField(max_length=50, blank=True)
    requisito_descripcion = models.CharField(max_length=200, blank=True)

    class Meta:
        managed = False
        db_table = 'requisito'


class RequisitosTramite(models.Model):
    tramite = models.ForeignKey('Tramite')
    requisito = models.ForeignKey(Requisito)

    class Meta:
        managed = False
        db_table = 'requisitos_tramite'


class Solicitud(models.Model):
    solicitud_id = models.AutoField(primary_key=True)
    usuario_envia = models.ForeignKey('Usuario', related_name='solicitud_usuario_envia')
    usuario_recibe = models.ForeignKey('Usuario', related_name='solicitud_usuario_recibe')
    solicitud_tipo = models.CharField(max_length=2, blank=True)
    solicitud_mensaje = models.CharField(max_length=500, blank=True)
    solicitud_fecha = models.DateTimeField(blank=True, null=True)
    solicitud_archivada = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'solicitud'


class TiempoUsuario(models.Model):
    usuario = models.ForeignKey('Usuario')
    actividad = models.ForeignKey(ActividadModalidad)
    modalidad_id = models.IntegerField()
    tiempo_usuario_minutos = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tiempo_usuario'


class Tramite(models.Model):
    tramite_id = models.AutoField(primary_key=True)
    organismo = models.ForeignKey(Organismo, blank=True, null=True)
    tramite_titulo = models.CharField(max_length=100, blank=True)
    tramite_descripcion = models.CharField(max_length=1000, blank=True)
    tramite_dur_anual = models.NullBooleanField()
    tramite_inicio = models.DateField(blank=True, null=True)
    tramite_fin = models.DateField(blank=True, null=True)
    tramite_resultado = models.CharField(max_length=300, blank=True)

    class Meta:
        managed = False
        db_table = 'tramite'


class TramiteModalidad(models.Model):
    tramite = models.ForeignKey(Tramite)
    modalidad = models.ForeignKey(Modalidad)

    class Meta:
        managed = False
        db_table = 'tramite_modalidad'


class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    comuna = models.ForeignKey(Comuna)
    usuario_nombre = models.CharField(max_length=50, blank=True)
    usuario_apellido_pat = models.CharField(max_length=25, blank=True)
    usuario_apellido_mat = models.CharField(max_length=25, blank=True)
    usuario_correo = models.CharField(max_length=50, blank=True)
    usuario_fecha_nacimiento = models.DateField(blank=True, null=True)
    usuario_password = models.CharField(max_length=25, blank=True)
    usuario_fecha_registro = models.DateField(blank=True, null=True)
    usuario_tipo = models.CharField(max_length=2, blank=True)
    usuario_estado = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'usuario'


class Valoracion(models.Model):
    usuario = models.ForeignKey(Usuario)
    oficina = models.ForeignKey(Oficina)
    valoracion_numero = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'valoracion'


class ViewActModTra(models.Model):
    tramite_id = models.IntegerField(blank=True, null=True)
    modalidad_tra_id = models.IntegerField(blank=True, null=True)
    tiempo_total = models.BigIntegerField(db_column='TIEMPO TOTAL', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dinero_total = models.BigIntegerField(db_column='DINERO TOTAL', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'view_act_mod_tra'


class ViewFormatoAct(models.Model):
    actividad_id = models.IntegerField(blank=True, null=True)
    modalidad_id = models.IntegerField(blank=True, null=True)
    modalidad_titulo = models.CharField(max_length=50, blank=True)
    documento_id = models.IntegerField(blank=True, null=True)
    formato_doc_nombre = models.CharField(max_length=2, blank=True)
    formato_doc_costo = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_formato_act'
