# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_traminet', '0002_auto_20141126_1114'),
    ]

    operations = [
        migrations.CreateModel(
            name='Relacionados',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relacionados_info', models.CharField(max_length=300, blank=True)),
            ],
            options={
                'db_table': 'relacionados',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ViewActModTra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tramite_id', models.IntegerField(null=True, blank=True)),
                ('modalidad_tra_id', models.IntegerField(null=True, blank=True)),
                ('tiempo_total', models.BigIntegerField(null=True, db_column='TIEMPO TOTAL', blank=True)),
                ('dinero_total', models.BigIntegerField(null=True, db_column='DINERO TOTAL', blank=True)),
            ],
            options={
                'db_table': 'view_act_mod_tra',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ViewFormatoAct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actividad_id', models.IntegerField(null=True, blank=True)),
                ('modalidad_id', models.IntegerField(null=True, blank=True)),
                ('modalidad_titulo', models.CharField(max_length=50, blank=True)),
                ('documento_id', models.IntegerField(null=True, blank=True)),
                ('formato_doc_nombre', models.CharField(max_length=2, blank=True)),
                ('formato_doc_costo', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'view_formato_act',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
