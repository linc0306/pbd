# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_traminet', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('actividad_id', models.IntegerField(serialize=False, primary_key=True)),
                ('actividad_titulo', models.CharField(max_length=100, blank=True)),
                ('actividad_descripcion', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'actividad',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActividadDoc',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actividad_doc_enoot', models.NullBooleanField()),
            ],
            options={
                'db_table': 'actividad_doc',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActividadModalidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mod_act_duracion', models.IntegerField(null=True, blank=True)),
                ('mod_act_costo', models.IntegerField(null=True, blank=True)),
                ('mod_act_pasos', models.CharField(max_length=500, blank=True)),
                ('mod_act_contacto', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'actividad_modalidad',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActividadOficina',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'actividad_oficina',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=80)),
            ],
            options={
                'db_table': 'auth_group',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthGroupPermissions',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'auth_group_permissions',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthPermission',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('codename', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'auth_permission',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('password', models.CharField(max_length=128)),
                ('last_login', models.DateTimeField()),
                ('is_superuser', models.BooleanField()),
                ('username', models.CharField(unique=True, max_length=30)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('email', models.CharField(max_length=75)),
                ('is_staff', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('date_joined', models.DateTimeField()),
            ],
            options={
                'db_table': 'auth_user',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUserGroups',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'auth_user_groups',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUserUserPermissions',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'auth_user_user_permissions',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('categoria_id', models.IntegerField(serialize=False, primary_key=True)),
                ('categoria_titulo', models.CharField(max_length=30, blank=True)),
                ('categoria_descripcion', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'categoria',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CategoriasTramite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'categorias_tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comuna',
            fields=[
                ('comuna_id', models.IntegerField(serialize=False, primary_key=True)),
                ('comuna_nombre', models.CharField(max_length=25, blank=True)),
            ],
            options={
                'db_table': 'comuna',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoAdminLog',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('action_time', models.DateTimeField()),
                ('object_id', models.TextField(blank=True)),
                ('object_repr', models.CharField(max_length=200)),
                ('action_flag', models.SmallIntegerField()),
                ('change_message', models.TextField()),
            ],
            options={
                'db_table': 'django_admin_log',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoContentType',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('app_label', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'django_content_type',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoSession',
            fields=[
                ('session_key', models.CharField(max_length=40, serialize=False, primary_key=True)),
                ('session_data', models.TextField()),
                ('expire_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_session',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Documento',
            fields=[
                ('documento_id', models.IntegerField(serialize=False, primary_key=True)),
                ('documento_titulo', models.CharField(max_length=100, blank=True)),
                ('documento_tipo', models.CharField(max_length=25, blank=True)),
                ('documento_descripcion', models.CharField(max_length=300, blank=True)),
            ],
            options={
                'db_table': 'documento',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DocumentoTramite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'documento_tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Edicion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edicion_fecha', models.DateField(null=True, blank=True)),
                ('edicion_tipo', models.CharField(max_length=2, blank=True)),
                ('edicion_descripcion', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'edicion',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etapa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modalidad_act_id', models.IntegerField()),
                ('modalidad_act_sig_id', models.IntegerField()),
                ('modalidad_tra_id', models.IntegerField()),
                ('etapa_nro', models.IntegerField(null=True, blank=True)),
                ('etapa_espera', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'etapa',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormatoDocumento',
            fields=[
                ('formato_doc_id', models.IntegerField(serialize=False, primary_key=True)),
                ('formato_doc_nombre', models.CharField(max_length=2, blank=True)),
                ('formato_doc_costo', models.IntegerField(null=True, blank=True)),
                ('formato_doc_archivo', models.CharField(max_length=254, blank=True)),
            ],
            options={
                'db_table': 'formato_documento',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ley',
            fields=[
                ('ley_id', models.IntegerField(serialize=False, primary_key=True)),
                ('ley_titulo', models.CharField(max_length=50, blank=True)),
                ('ley_descripcion', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'ley',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LogSql',
            fields=[
                ('log_id', models.IntegerField(serialize=False, primary_key=True)),
                ('log_fecha', models.DateField(null=True, blank=True)),
                ('log_tabla', models.CharField(max_length=30, blank=True)),
                ('log_usuario', models.CharField(max_length=100, blank=True)),
                ('log_operacion', models.CharField(max_length=8, blank=True)),
                ('log_antes', models.CharField(max_length=1000, blank=True)),
                ('log_despues', models.CharField(max_length=1000, blank=True)),
            ],
            options={
                'db_table': 'log_sql',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Marcado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('marcado_fecha', models.DateField(null=True, blank=True)),
                ('marcado_notificacion', models.NullBooleanField()),
            ],
            options={
                'db_table': 'marcado',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MarcoLegalTramite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'marco_legal_tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Modalidad',
            fields=[
                ('modalidad_id', models.IntegerField(serialize=False, primary_key=True)),
                ('modalidad_titulo', models.CharField(max_length=50, blank=True)),
                ('modalidad_descripcion', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'db_table': 'modalidad',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Notificacion',
            fields=[
                ('notificacion_id', models.IntegerField(serialize=False, primary_key=True)),
                ('notificacion_titulo', models.CharField(max_length=50, blank=True)),
                ('notificacion_descripcion', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'db_table': 'notificacion',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Oficina',
            fields=[
                ('oficina_id', models.IntegerField(serialize=False, primary_key=True)),
                ('oficina_nombre', models.CharField(max_length=100, blank=True)),
                ('oficina_direccion', models.CharField(max_length=150, blank=True)),
                ('oficina_tel_fijo', models.CharField(max_length=10, blank=True)),
                ('oficina_tel_celu', models.CharField(max_length=10, blank=True)),
                ('oficina_horario', models.CharField(max_length=150, blank=True)),
            ],
            options={
                'db_table': 'oficina',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrdenTrabajo',
            fields=[
                ('orden_id', models.IntegerField(serialize=False, primary_key=True)),
                ('orden_asunto', models.CharField(max_length=100, blank=True)),
                ('orden_descripcion', models.CharField(max_length=500, blank=True)),
                ('orden_fecha', models.DateField(null=True, blank=True)),
                ('orden_fecha_entrega', models.DateField(null=True, blank=True)),
                ('orden_estado', models.CharField(max_length=2, blank=True)),
            ],
            options={
                'db_table': 'orden_trabajo',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organismo',
            fields=[
                ('organismo_id', models.IntegerField(serialize=False, primary_key=True)),
                ('organismo_nombre', models.CharField(max_length=100, blank=True)),
                ('organismo_descripcion', models.CharField(max_length=300, blank=True)),
                ('organismo_tel_fijo', models.CharField(max_length=10, blank=True)),
                ('organismo_tel_celu', models.CharField(max_length=10, blank=True)),
                ('organismo_pagina_web', models.CharField(max_length=50, blank=True)),
                ('organismo_logo', models.CharField(max_length=254, blank=True)),
            ],
            options={
                'db_table': 'organismo',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PalabraClave',
            fields=[
                ('palabra_id', models.IntegerField(serialize=False, primary_key=True)),
                ('palabra_nombre', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'palabra_clave',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PalabraFichaTramite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'palabra_ficha_tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Provincia',
            fields=[
                ('provincia_id', models.IntegerField(serialize=False, primary_key=True)),
                ('provincia_nombre', models.CharField(max_length=30, blank=True)),
            ],
            options={
                'db_table': 'provincia',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('region_id', models.IntegerField(serialize=False, primary_key=True)),
                ('region_nombre', models.CharField(max_length=60, blank=True)),
            ],
            options={
                'db_table': 'region',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Requisito',
            fields=[
                ('requisito_id', models.IntegerField(serialize=False, primary_key=True)),
                ('requisito_titulo', models.CharField(max_length=50, blank=True)),
                ('requisito_descripcion', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'requisito',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RequisitosTramite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'requisitos_tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Solicitud',
            fields=[
                ('solicitud_id', models.IntegerField(serialize=False, primary_key=True)),
                ('solicitud_tipo', models.CharField(max_length=2, blank=True)),
                ('solicitud_mensaje', models.CharField(max_length=500, blank=True)),
                ('solicitud_fecha', models.DateField(null=True, blank=True)),
                ('solicitud_archivada', models.NullBooleanField()),
            ],
            options={
                'db_table': 'solicitud',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TiempoUsuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modalidad_id', models.IntegerField()),
                ('tiempo_usuario_minutos', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'tiempo_usuario',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tramite',
            fields=[
                ('tramite_id', models.IntegerField(serialize=False, primary_key=True)),
                ('tramite_titulo', models.CharField(max_length=100, blank=True)),
                ('tramite_descripcion', models.CharField(max_length=500, blank=True)),
                ('tramite_descripcion_duracion', models.CharField(max_length=100, blank=True)),
                ('tramite_inicio', models.DateField(null=True, blank=True)),
                ('tramite_fin', models.DateField(null=True, blank=True)),
                ('tramite_resultado', models.CharField(max_length=300, blank=True)),
            ],
            options={
                'db_table': 'tramite',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FichaTecnica',
            fields=[
                ('tramite', models.ForeignKey(primary_key=True, serialize=False, to='app_traminet.Tramite')),
                ('ficha_numero_visitas', models.IntegerField(null=True, blank=True)),
                ('ficha_fecha_creacion', models.DateField(null=True, blank=True)),
                ('ficha_fecha_actualizado', models.DateField(null=True, blank=True)),
                ('ficha_estado', models.CharField(max_length=2, blank=True)),
                ('ficha_comentario_estado', models.CharField(max_length=200, blank=True)),
                ('ficha_destacado', models.NullBooleanField()),
            ],
            options={
                'db_table': 'ficha_tecnica',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TramiteModalidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'tramite_modalidad',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('usuario_id', models.IntegerField(serialize=False, primary_key=True)),
                ('usuario_nombre', models.CharField(max_length=50, blank=True)),
                ('usuario_apellido_pat', models.CharField(max_length=25, blank=True)),
                ('usuario_apellido_mat', models.CharField(max_length=25, blank=True)),
                ('usuario_rut', models.CharField(max_length=10, blank=True)),
                ('usuario_correo', models.CharField(max_length=50, blank=True)),
                ('usuario_fecha_nacimiento', models.DateField(null=True, blank=True)),
                ('usuario_password', models.CharField(max_length=25, blank=True)),
                ('usuario_fecha_registro', models.DateField(null=True, blank=True)),
                ('usuario_tipo', models.CharField(max_length=2, blank=True)),
                ('usuario_estado', models.NullBooleanField()),
            ],
            options={
                'db_table': 'usuario',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Valoracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valoracion_numero', models.SmallIntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'valoracion',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Prueba',
        ),
    ]
