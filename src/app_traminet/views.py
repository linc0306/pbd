# -*- encoding: utf-8 -*-

from django.shortcuts import render, render_to_response, get_object_or_404, RequestContext, HttpResponseRedirect
from django.http import HttpResponse
from django.core.mail import EmailMessage
from django.db.models import Avg


# Importacion de las clases del modelo
from app_traminet.models import *


# Esta importacion permite el uso del cursor
from django.db import connection 

#Importacion librerias login
from django.contrib.auth.decorators import login_required
from django.contrib import auth 
from django.core.context_processors import csrf
from django.contrib.auth import login as django_login, authenticate, logout as django_logout


# Importacion de formularios encontrados en /forms
from app_traminet.forms import MyRegistrationForm

# para vistas que necesiten permisos
from django.contrib.auth.decorators import permission_required


# Vistas

def home(request):
    return render_to_response("home.html", 
                               locals(),
                               context_instance = RequestContext(request))

def nosotros(request):
    return render_to_response("nosotros.html", 
                               locals(),
                               context_instance = RequestContext(request)) 
    
def registro(request):
    return render_to_response("registro.html", 
                               locals(),
                               context_instance = RequestContext(request))

def tramites(request):

    lista_tramites = []

    lista_id_tramite = []

    palabra = request.GET.get('search')
  
    if palabra:

        cursor = connection.cursor()

        try:
    	    cursor.execute("SELECT tramite_id FROM tramite WHERE tramite_titulo ILIKE '%%%s%%'" % palabra)

    	    lista_id_tramite = cursor.fetchall()
            largo_lista = len(lista_id_tramite)

            for i in range(largo_lista):

                tramite = Tramite.objects.get(tramite_id = lista_id_tramite[i][0]) 
                lista_tramites.append(tramite)
        finally:
            cursor.close()

    else:

        queryset = list(Tramite.objects.all())

        for elemento in queryset:
            lista_tramites.append(elemento)        

    return render_to_response ("tramites.html",
                               {"lista_tramites":lista_tramites},
                               context_instance = RequestContext(request))

def listado_organismos(request):

    lista_organismos = []

    lista_id_organismo = []

    palabra = request.GET.get('search')
  
    if palabra:

        cursor = connection.cursor()

        try:
    	    cursor.execute("SELECT organismo_id FROM organismo WHERE organismo_nombre ILIKE '%%%s%%'" % palabra)

    	    lista_id_organismo = cursor.fetchall()
            largo_lista = len(lista_id_organismo)

            for i in range(largo_lista):

                organismo = Organismo.objects.get(organismo_id = lista_id_organismo[i][0]) 
                lista_organismos.append(organismo)

        finally:
            cursor.close()

    else:

        queryset = list(Organismo.objects.all())
    
        for elemento in queryset:
            lista_organismos.append(elemento)        

    return render_to_response ("organismos.html",
                               {"lista_organismos":lista_organismos},
                               context_instance = RequestContext(request))


def listado_categorias_tramite(request, argumento):
    
    lista_tramites = []
    lista_id_tramite = []
    
    id_categoria = int(argumento)

    cursor = connection.cursor()

    try:
      cursor.execute("SELECT tramite_id FROM categorias_tramite WHERE categoria_id = %s", [id_categoria])

      lista_id_tramite = cursor.fetchall()
      largo_lista = len(lista_id_tramite)

      cursor.execute("SELECT categoria_titulo FROM categoria WHERE categoria_id = %s", [id_categoria])
      nombre_categoria = cursor.fetchall()    

      for i in range(largo_lista):

        tramite = Tramite.objects.get(tramite_id = lista_id_tramite[i][0]) 
        lista_tramites.append(tramite)

    finally:
        cursor.close()
   
    return render_to_response ("listar_tramite.html",
                               {"lista_tramites":lista_tramites, "nombre_categoria": nombre_categoria[0]},
                               context_instance = RequestContext(request))

def listado_categorias(request):
    #lista_categorias = []
    #lista_img_categorias = []
    queryset = list(Categoria.objects.all())

    matriz_categorias = []

    for elemento in queryset:

      fila_categorias = []

      fila_categorias.append(elemento)
      nombre_img = elemento.categoria_titulo + ".png"
      fila_categorias.append(nombre_img)

      matriz_categorias.append(fila_categorias)

    return render_to_response ("categorias.html",
                               {"matriz_categorias": matriz_categorias},
                               context_instance = RequestContext(request))

def listado_documentos(request):

    lista_documentos = []

    lista_id_documento = []

    palabra = request.GET.get('search')
  
    if palabra:

        cursor = connection.cursor()

        try:
    	    cursor.execute("SELECT documento_id FROM documento WHERE documento_titulo ILIKE '%%%s%%'" % palabra)

    	    lista_id_documento = cursor.fetchall()
            largo_lista = len(lista_id_documento)

            for i in range(largo_lista):

                documento = Documento.objects.get(documento_id = lista_id_documento[i][0]) 
                lista_documentos.append(documento)

        finally:
            cursor.close()

    else:
        queryset = list(Documento.objects.all())

        for elemento in queryset:
            lista_documentos.append(elemento)        

    return render_to_response ("documentos.html",
                               {"lista_documentos":lista_documentos},
                               context_instance = RequestContext(request))

def listado_mas_visitados(request):
  tramites_mas_visitados = []  

  queryset = list(FichaTecnica.objects.order_by('-ficha_numero_visitas')[:3])

  for elemento in queryset:

    tramite = Tramite.objects.get(tramite_id = elemento.tramite_id)

    tramites_mas_visitados.append(tramite);

  return render_to_response("mas_visitados.html",
                            {"tramites_mas_visitados": tramites_mas_visitados},
                            context_instance = RequestContext(request))  

def listado_destacados(request):
  tramites_destacados = []  

  queryset = list(FichaTecnica.objects.filter(ficha_destacado = True))

  for elemento in queryset:

    tramite = Tramite.objects.get(tramite_id = elemento.tramite_id)

    tramites_destacados.append(tramite);

  return render_to_response("destacados.html",
                            {"tramites_destacados": tramites_destacados},
                            context_instance = RequestContext(request))  

def listado_categorias_tramite(request, argumento):
    
    lista_tramites = []
    lista_id_tramite = []
    
    id_categoria = int(argumento)

    cursor = connection.cursor()

    try:
      cursor.execute("SELECT tramite_id FROM categorias_tramite WHERE categoria_id = %s", [id_categoria])

      lista_id_tramite = cursor.fetchall()
      largo_lista = len(lista_id_tramite)

      cursor.execute("SELECT categoria_titulo FROM categoria WHERE categoria_id = %s", [id_categoria])
      nombre_categoria = cursor.fetchall()
    
      ''' ESTE ES UN COMENTARIO
      Se hace este for, porque lo que devuelve el cursor(fetchall), es algo como esto:
      ((1,)(2,)(3,))
      y por medio del for pude acceder a los tramite_id que obtuve en el 
        '''
      for i in range(largo_lista):

        tramite = Tramite.objects.get(tramite_id = lista_id_tramite[i][0]) 
        lista_tramites.append(tramite)

    finally:
      cursor.close()
   
    return render_to_response ("listar_tramite.html",
                               {"lista_tramites":lista_tramites, "nombre_categoria": nombre_categoria[0]},
                               context_instance = RequestContext(request))

def mostrar_tramite(request, argumento):

  id_tramite = int(argumento)  

  relacionados = []
  documentos = []
  requisitos = []
  leyes = []  
  modalidades = []  

  tramite = Tramite.objects.get(tramite_id = id_tramite)

  organismo = Organismo.objects.get(organismo_id = tramite.organismo_id)  

  cursor = connection.cursor()

  try:
    #lista relacionados
    cursor.execute("SELECT * FROM relacionados WHERE tramite_id = %s", [id_tramite])

    lista_relacionados = cursor.fetchall()
    largo_lista = len(lista_relacionados)

    for i in range(largo_lista):
      relacionado = Relacionados.objects.get(tramite_rel_id = lista_relacionados[i][1])
      relacionados.append(relacionado)

    cursor.execute("SELECT * FROM documento_tramite WHERE tramite_id = %s", [id_tramite])

    lista_documentos = cursor.fetchall();
    largo_lista = len(lista_documentos)

    for i in range(largo_lista):      
      documento = Documento.objects.get(documento_id = lista_documentos[i][1])      
      documentos.append(documento)

    cursor.execute("SELECT * FROM requisitos_tramite WHERE tramite_id = %s", [id_tramite])

    lista_requisitos = cursor.fetchall()
    largo_lista = len(lista_requisitos)

    for i in range(largo_lista):
      requisito = Requisito.objects.get(requisito_id = lista_requisitos[i][1])
      requisitos.append(requisito)

    cursor.execute("SELECT * FROM marco_legal_tramite WHERE tramite_id = %s", [id_tramite])

    lista_leyes = cursor.fetchall()
    largo_lista = len(lista_leyes)    

    for i in range(largo_lista):
      ley = Ley.objects.get(ley_id = lista_leyes[i][1])
      leyes.append(ley)

    cursor.execute("SELECT * FROM tramite_modalidad WHERE tramite_id = %s", [id_tramite])

    lista_modalidades = cursor.fetchall()
    largo_lista = len(lista_modalidades)    

    for i in range(largo_lista):
      modalidad = Modalidad.objects.get(modalidad_id = lista_modalidades[i][1])
      modalidades.append(modalidad)
            
  finally:
    cursor.close()      

  return render_to_response ("pag_tramite.html", 
                                 {"tramite": tramite, "organismo": organismo, 
                                 "relacionados": relacionados, "documentos": documentos,
                                "requisitos": requisitos, "leyes": leyes, "modalidades": modalidades},
                               context_instance = RequestContext(request))

def mostrar_modalidades(request, argumento1, argumento2):

  id_tramite = int(argumento1)
  id_modalidad = int (argumento2)

  relacionados = []
  documentos = []
  requisitos = []
  leyes = []  
  modalidades = []  

  tramite = Tramite.objects.get(tramite_id = id_tramite)

  organismo = Organismo.objects.get(organismo_id = tramite.organismo_id)  

  cursor = connection.cursor()

  try:
    #lista relacionados
    cursor.execute("SELECT * FROM relacionados WHERE tramite_id = %s", [id_tramite])

    lista_relacionados = cursor.fetchall()
    largo_lista = len(lista_relacionados)

    for i in range(largo_lista):
      relacionado = Relacionados.objects.get(tramite_rel_id = lista_relacionados[i][1])
      relacionados.append(relacionado)

    cursor.execute("SELECT * FROM documento_tramite WHERE tramite_id = %s", [id_tramite])

    lista_documentos = cursor.fetchall();
    largo_lista = len(lista_documentos)

    for i in range(largo_lista):      
      documento = Documento.objects.get(documento_id = lista_documentos[i][1])      
      documentos.append(documento)

    cursor.execute("SELECT * FROM requisitos_tramite WHERE tramite_id = %s", [id_tramite])

    lista_requisitos = cursor.fetchall()
    largo_lista = len(lista_requisitos)

    for i in range(largo_lista):
      requisito = Requisito.objects.get(requisito_id = lista_requisitos[i][1])
      requisitos.append(requisito)

    cursor.execute("SELECT * FROM marco_legal_tramite WHERE tramite_id = %s", [id_tramite])

    lista_leyes = cursor.fetchall()
    largo_lista = len(lista_leyes)    

    for i in range(largo_lista):
      ley = Ley.objects.get(ley_id = lista_leyes[i][1])
      leyes.append(ley)

    cursor.execute("SELECT * FROM tramite_modalidad WHERE tramite_id = %s", [id_tramite])

    lista_modalidades = cursor.fetchall()
    largo_lista = len(lista_modalidades)    

    for i in range(largo_lista):
      modalidad = Modalidad.objects.get(modalidad_id = lista_modalidades[i][1])
      modalidades.append(modalidad)
      
    matriz_etapa_act_mod = []
    

    cursor.execute("SELECT * FROM etapa WHERE tramite_id = %s AND modalidad_act_id = %s ORDER BY etapa_nro", [id_tramite, id_modalidad])
    lista_etapas = cursor.fetchall()
    largo_lista = len(lista_etapas)

    for i in range(largo_lista):
      cursor.execute("SELECT * FROM actividad_modalidad WHERE actividad_id =%s AND modalidad_id = %s", [lista_etapas[i][0], lista_etapas[i][1] ])
      lista_act_mod = cursor.fetchall()

      oficinas = []      
      fila_etapa_act_mod = []
      fila_etapa_act_mod.append(lista_etapas[i])      
      fila_etapa_act_mod.append(lista_act_mod[0])

      actividad = Actividad.objects.get(actividad_id = lista_act_mod[0][0])
      modalidad = Modalidad.objects.get(modalidad_id = lista_act_mod[0][1])

      cursor.execute("SELECT documento_titulo  FROM actividad_doc, documento WHERE documento.documento_id = actividad_doc.documento_id AND actividad_id = %s", [lista_act_mod[0][0]])
      documentos_actividad = cursor.fetchall()

      cursor.execute("SELECT oficina_id FROM actividad_oficina WHERE actividad_id = %s", [lista_act_mod[0][0]])
      ids_oficinas = cursor.fetchall()
      largo_ids = len(ids_oficinas)


      for j in range(largo_ids):
        oficina = Oficina.objects.get(oficina_id = ids_oficinas[j][0])
        oficinas.append(oficina)

      fila_etapa_act_mod.append(actividad)
      fila_etapa_act_mod.append(modalidad)

      fila_etapa_act_mod.append(documentos_actividad)
      fila_etapa_act_mod.append(oficinas)

      matriz_etapa_act_mod.append(fila_etapa_act_mod)        

  finally:
    cursor.close()      

  return render_to_response ("pag_tramite_modalidad.html", 
                                 {"tramite": tramite, "organismo": organismo, 
                                 "relacionados": relacionados, "documentos": documentos,
                                "requisitos": requisitos, "leyes": leyes, "modalidades": modalidades,
                                "matriz_etapa_act_mod": matriz_etapa_act_mod},
                               context_instance = RequestContext(request))

def mostrar_organismo(request, argumento):

  id_organismo = int(argumento)

  organismo = Organismo.objects.get(organismo_id = id_organismo )

  comunas = Comuna.objects.all()
  provincias = Provincia.objects.all()
  regiones = Region.objects.all()

  return render_to_response ("pag_organismo.html", {"organismo": organismo, 
                              "comunas": comunas, "provincias": provincias,                                
                               "regiones": regiones},
                               context_instance = RequestContext(request))

def mostrar_documento(request, argumento):

  id_documento = int(argumento)

  documento = Documento.objects.get(documento_id = id_documento )

  formatos = FormatoDocumento.objects.filter(documento = id_documento)

  return render_to_response ("pag_documento.html",
                                 {"documento": documento, "formatos": formatos},
                               context_instance = RequestContext(request))

def auditor(request):

    lista_tabla = []

    cursor = connection.cursor()
    
    try:
    		    
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public' order by table_name")
	lista = cursor.fetchall()
	largo_lista = len(lista)

	for i in range(largo_lista):
      	  tabla = lista[i][0]
	  lista_tabla.append(tabla)


    finally:
    	cursor.close()

    return render_to_response("consulta_auditor.html", 
                               {"lista_tabla": lista_tabla},
                               context_instance = RequestContext(request))

def auditor_limpiar(request):

    lista_tabla = []

    cursor = connection.cursor()
    
    try:
    		    
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public' order by table_name")
	lista = cursor.fetchall()
	largo_lista = len(lista)

	for i in range(largo_lista):
      	  tabla = lista[i][0]
	  lista_tabla.append(tabla)


    finally:
    	cursor.close()

    return render_to_response("limpiar_auditor.html", 
                               {"lista_tabla": lista_tabla},
                               context_instance = RequestContext(request))

def auditor_configuracion(request):

    lista_tabla = []

    cursor = connection.cursor()
    
    try:
    		    
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public' order by table_name")
	lista = cursor.fetchall()
	largo_lista = len(lista)

	for i in range(largo_lista):
      	  tabla = lista[i][0]
	  lista_tabla.append(tabla)


    finally:
    	cursor.close()

    return render_to_response("configuracion_auditor.html", 
                               {"lista_tabla": lista_tabla},
                               context_instance = RequestContext(request))

def admin_sitio(request):

    lista_tramites = []
    lista_usuarios = []
    lista_fichas = []

    if request.POST:
        cursor = connection.cursor()
    
        try:
            cursor.execute("SELECT tramite_id FROM tramite")
	    lista = cursor.fetchall()
	    largo_lista = len(lista)

	    for i in range(largo_lista):
              variable = str(lista[i][0]) + "_d"
	      palabra = request.POST.get(variable)

              if palabra:
	        f = FichaTecnica.objects.get(tramite_id = lista[i][0])
	        f.ficha_destacado = True
		f.save()
	      else:
		f = FichaTecnica.objects.get(tramite_id = lista[i][0])
	        f.ficha_destacado = False
		f.save()


        finally:
            cursor.close()

        queryset = list(Tramite.objects.all())

        for elemento in queryset:
            lista_tramites.append(elemento)

        queryset = list(Usuario.objects.all())

        for elemento in queryset:
            lista_usuarios.append(elemento)

        queryset = list(FichaTecnica.objects.all())

        for elemento in queryset:
            lista_fichas.append(elemento)

    else:
        queryset = list(Tramite.objects.all())

        for elemento in queryset:
            lista_tramites.append(elemento)

        queryset = list(Usuario.objects.all())

        for elemento in queryset:
            lista_usuarios.append(elemento)

        queryset = list(FichaTecnica.objects.all())

        for elemento in queryset:
            lista_fichas.append(elemento)
    

    return render_to_response("administrar_sitio.html", 
                               {"lista_tramites": lista_tramites,
				"lista_usuarios": lista_usuarios,
				"lista_fichas": lista_fichas},
                               context_instance = RequestContext(request))

def organismos_ai(request):

    lista_organismos = []

    lista_id_organismo = []

    palabra = request.GET.get('search')
  
    if palabra:

        cursor = connection.cursor()

        try:
    	    cursor.execute("SELECT organismo_id FROM organismo WHERE organismo_nombre ILIKE '%%%s%%'" % palabra)

    	    lista_id_organismo = cursor.fetchall()
            largo_lista = len(lista_id_organismo)

            for i in range(largo_lista):

                organismo = Organismo.objects.get(organismo_id = lista_id_organismo[i][0]) 
                lista_organismos.append(organismo)

        finally:
            cursor.close()

    else:

        queryset = list(Organismo.objects.all())
    
        for elemento in queryset:
            lista_organismos.append(elemento)        

    return render_to_response ("organismos_ai.html",
                               {"lista_organismos":lista_organismos},
                               context_instance = RequestContext(request))

def agregar_organismo(request):

    if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')
	telefono_fijo = request.POST.get('telefono_fijo')
	celular = request.POST.get('celular')
	pagina = request.POST.get('pagina')

        o = Organismo(organismo_nombre = nombre,
			organismo_descripcion = descripcion,
			organismo_tel_fijo = telefono_fijo,
			organismo_tel_celu = celular,
			organismo_pagina_web = pagina)
	o.save()

    return render_to_response ("agregar_organismo.html",
                                 locals(),
                               context_instance = RequestContext(request))

def documentos_ai(request):

    lista_documentos = []

    lista_id_documento = []

    palabra = request.GET.get('search')
  
    if palabra:

        cursor = connection.cursor()

        try:
    	    cursor.execute("SELECT documento_id FROM documento WHERE documento_titulo ILIKE '%%%s%%'" % palabra)

    	    lista_id_documento = cursor.fetchall()
            largo_lista = len(lista_id_documento)

            for i in range(largo_lista):

                documento = Documento.objects.get(documento_id = lista_id_documento[i][0]) 
                lista_documentos.append(documento)

        finally:
            cursor.close()

    else:
        queryset = list(Documento.objects.all())

        for elemento in queryset:
            lista_documentos.append(elemento)        

    return render_to_response ("documentos_ai.html",
                               {"lista_documentos":lista_documentos},
                               context_instance = RequestContext(request))

def agregar_documento(request):

   if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')
	tipo = request.POST.get('tipo')
	virtual = request.POST.get('virtual')
	fisico = request.POST.get('fisico')
	costo_virtual = request.POST.get('costo_virtual')
	costo_fisico = request.POST.get('costo_fisico')
	
	

        d = Documento(documento_titulo = nombre,
			documento_tipo = tipo,
			documento_descripcion = descripcion)
	d.save()
	if virtual:
		f = FormatoDocumento(documento_id = d.documento_id,
				formato_doc_nombre = "V",
				formato_doc_costo = int(costo_virtual))
		f.save()

	if fisico:
		f = FormatoDocumento(documento_id = d.documento_id,
				formato_doc_nombre = "F",
				formato_doc_costo = int(costo_fisico))
		f.save()

   return render_to_response ("agregar_documento.html",
                               locals(),
                               context_instance = RequestContext(request))

def agregar_oficina(request):

   if request.POST:

	nombre = request.POST.get('nombre')
	direccion = request.POST.get('direccion')
	telefono_fijo = request.POST.get('telefono_fijo')
	celular = request.POST.get('celular')
	horario = request.POST.get('horario')
	id_organismo = request.POST.get('id_organismo')
	id_comuna = request.POST.get('id_comuna')
	
	print nombre, direccion, telefono_fijo, celular, horario, id_organismo, id_comuna

	o = Oficina(comuna_id = id_comuna,
		organismo_id = id_organismo,
		oficina_nombre = nombre,
		oficina_direccion = direccion,
		oficina_tel_fijo = telefono_fijo,
		oficina_tel_celu = celular,
		oficina_horario = horario)
	o.save()

	organismos = Organismo.objects.all()
	comunas = Comuna.objects.all()

   else:

	organismos = Organismo.objects.all()
	comunas = Comuna.objects.all()

   return render_to_response ("agregar_oficina.html",
                               {"organismos":organismos,
				"comunas":comunas},
                               context_instance = RequestContext(request))

def agregar_actividad(request):

    if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')

	a = Actividad(actividad_titulo = nombre,
	 	      actividad_descripcion = descripcion)
	
	a.save()

	modalidades = Modalidad.objects.all()
	documentos = Documento.objects.all()
	oficinas = Oficina.objects.all()

	cursor = connection.cursor()

        try:    	    
	    largo_lista = len(modalidades)

	    for i in range(1, largo_lista):
                variable = str(i) + "_m"		
	      	palabra = request.POST.get(variable)
                variable = str(i) + "_duracion"
		duracion = request.POST.get(variable)
		variable = str(i) + "_costo"
		costo = request.POST.get(variable)
                variable = str(i) + "_paso"
		paso = request.POST.get(variable)
		variable = str(i) + "_contacto"
		contacto = request.POST.get(variable)
		variable = str(i) + "_nota"
		nota = request.POST.get(variable)
	      
	        if palabra:
		  	cursor.execute("INSERT INTO actividad_modalidad VALUES (%s, %s, %s, %s, %s, %s, %s )" , [a.actividad_id, i, duracion, costo, paso, contacto, 				nota])

	    largo_lista = len(documentos) + 1

	    for i in range(1, largo_lista):
            	variable = str(i) + "_d"
	    	palabra = request.POST.get(variable)
		variable = str(i) + "_entrega"
		entrega = request.POST.get(variable)

	        if palabra:
			cursor.execute("INSERT INTO actividad_doc VALUES (%s, %s, %s )" , [a.actividad_id, i, entrega])

	    largo_lista = len(oficinas) + 1

	    for i in range(1, largo_lista):
              	variable = str(i) + "_o"
	      	palabra = request.POST.get(variable)

	        if palabra:
			cursor.execute("INSERT INTO actividad_oficina(actividad_id, oficina_id) VALUES (%s, %s )" , [a.actividad_id, i])

	finally:
            cursor.close()

    else:
	modalidades = Modalidad.objects.all()
	documentos = Documento.objects.all()
	oficinas = Oficina.objects.all()

    return render_to_response ("agregar_actividad.html",
                                 {"modalidades": modalidades,
				"documentos": documentos,
				"oficinas": oficinas},
                               context_instance = RequestContext(request))

def agregar_ley(request):

    if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')

        l = Ley(ley_titulo = nombre,
		ley_descripcion = descripcion)
	l.save()

    return render_to_response ("agregar_ley.html",
                                 locals(),
                               context_instance = RequestContext(request))

def agregar_requisito(request):

    if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')

        r = Requisito(requisito_titulo = nombre,
			requisito_descripcion = descripcion)
	r.save()


    return render_to_response ("agregar_requisito.html",
                                 locals(),
                               context_instance = RequestContext(request))

def agregar_tramite(request):

    requisitos = Requisito.objects.all()
    documentos = Documento.objects.all()
    leyes = Ley.objects.all()
    organismos = Organismo.objects.all()
    modalidades = Modalidad.objects.all()
    categorias = Categoria.objects.all()

    if request.POST:

	nombre = request.POST.get('nombre')
	descripcion = request.POST.get('descripcion')
	dur_anual = request.POST.get('dur_anual')
	inicio = request.POST.get('inicio')
	fin = request.POST.get('fin')
	resultado = request.POST.get('resultado')
	if inicio and fin:
	    t = Tramite(tramite_titulo = nombre,
		 	    tramite_descripcion = descripcion,
			    tramite_dur_anual = dur_anual,
			    tramite_inicio = inicio,
			    tramite_fin = fin,
			    tramite_resultado = resultado)
	
            t.save()
	else:
	    t = Tramite(tramite_titulo = nombre,
	 	    tramite_descripcion = descripcion,
		    tramite_dur_anual = dur_anual,
		    tramite_resultado = resultado)
	
	    t.save()

	cursor = connection.cursor()

        try:    	    
	    largo_lista = len(requisitos) +1

	    for i in range(1, largo_lista):
                variable = str(i) + "_r"		
	      	palabra = request.POST.get(variable)              
	      
	        if palabra:
		  	cursor.execute("INSERT INTO requisitos_tramite VALUES (%s, %s)" , [t.tramite_id, i])

	    largo_lista = len(documentos) + 1

	    for i in range(1, largo_lista):
            	variable = str(i) + "_d"
	    	palabra = request.POST.get(variable)              
	      
	        if palabra:
		  	cursor.execute("INSERT INTO documento_tramite VALUES (%s, %s)" , [t.tramite_id, i])

	    largo_lista = len(leyes) + 1

	    for i in range(1, largo_lista):
              	variable = str(i) + "_l"
	      	palabra = request.POST.get(variable)

	        if palabra:
			cursor.execute("INSERT INTO marco_legal_tramite VALUES (%s, %s )" , [t.tramite_id, i])

	    largo_lista = len(organismos) + 1

	    for i in range(1, largo_lista):
              	variable = str(i) + "_o"
	      	palabra = request.POST.get(variable)

	        if palabra:
			t.organismo_id = i
			t.save()

	    largo_lista = len(categorias) + 1

	    for i in range(1, largo_lista):
              	variable = str(i) + "_c"
	      	palabra = request.POST.get(variable)

	        if palabra:
			cursor.execute("INSERT INTO categorias_tramite VALUES (%s, %s )" , [t.tramite_id, i])
	    
	    largo_lista = len(modalidades)

	    for i in range(1, largo_lista):
              	variable = str(i) + "_m"
	      	palabra = request.POST.get(variable)

	        if palabra:
			cursor.execute("INSERT INTO tramite_modalidad VALUES (%s, %s )" , [t.tramite_id, i])

	finally:
            cursor.close()




    return render_to_response ("agregar_tramite.html",
                                 {"requisitos": requisitos,
				  "documentos": documentos,
				  "leyes": leyes,
				  "organismos": organismos,
				  "modalidades": modalidades,
				  "categorias": categorias},
                               context_instance = RequestContext(request))




# Vistas relacionadas login

def login(request):
  c={}
  c.update(csrf(request))
  return render_to_response('login.html', c)

def auth_view(request):
  #Username para nosotros sera el mail
  username=request.POST.get('username', '')
  password = request.POST.get('password', '')
  #user = auth.authenticate(email=email, password=password) #Retorna users con mismo mail y pass
  user = authenticate(username=username, password=password) #Retorna users con mismo mail y pass

  #Si user no esta vacio
  if user is not None: 
    auth.login(request, user)
    return HttpResponseRedirect('/accounts/loggedin')

  else:
    return HttpResponseRedirect('/accounts/invalid')

@login_required
def loggedin(request):
  return render_to_response('loggedin.html',
                            {'full_name': request.user.username})

def invalid_login(request):
  return render_to_response('invalid_login.html')


@login_required
def logout(request):
    auth.logout(request)
    #return redirect('/') #o logout.html?
    return render_to_response("home.html")


# Vistas para el registro de usuario
 
def register_user(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            #new_user = form.save()
            form.save()
            return HttpResponseRedirect("/accounts/register_sucess")
    args={}
    args.update(csrf(request))
    args['form'] = MyRegistrationForm()
    return render_to_response('register.html', args)

def register_sucess(request):
    return render_to_response('register_sucess.html') 


# Manejo de error 404
def handler404(request):
    return render(request, '404.html')

# Manejo de error 500
def handler500(request):
    return render(request, '500.html')
