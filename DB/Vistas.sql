CREATE OR REPLACE VIEW VIEW_FORMATO_ACT
AS
	SELECT 	ACTIVIDAD.ACTIVIDAD_ID, MODALIDAD.MODALIDAD_ID, MODALIDAD_TITULO, DOCUMENTO.DOCUMENTO_ID, FORMATO_DOC_NOMBRE, FORMATO_DOC_COSTO
	FROM 	MODALIDAD, ACTIVIDAD_MODALIDAD, ACTIVIDAD, ACTIVIDAD_DOC, DOCUMENTO, FORMATO_DOCUMENTO
	WHERE 	MODALIDAD.MODALIDAD_ID = ACTIVIDAD_MODALIDAD.MODALIDAD_ID AND ACTIVIDAD.ACTIVIDAD_ID = ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID AND
			ACTIVIDAD_DOC.ACTIVIDAD_ID = ACTIVIDAD.ACTIVIDAD_ID AND ACTIVIDAD_DOC.DOCUMENTO_ID = DOCUMENTO.DOCUMENTO_ID AND
			FORMATO_DOCUMENTO.DOCUMENTO_ID = DOCUMENTO.DOCUMENTO_ID;

CREATE OR REPLACE VIEW VIEW_ACT_MOD_TRA
AS
	SELECT 	TRAMITE_ID, MODALIDAD_TRA_ID, SUM(MOD_ACT_DURACION) AS "TIEMPO TOTAL", SUM(MOD_ACT_COSTO) AS "DINERO TOTAL"
	FROM 	ETAPA, ACTIVIDAD_MODALIDAD
	WHERE 	ETAPA.ACTIVIDAD_ID = ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID AND ETAPA.MODALIDAD_ACT_ID = ACTIVIDAD_MODALIDAD.MODALIDAD_ID
	GROUP BY TRAMITE_ID , MODALIDAD_TRA_ID
