﻿INSERT INTO actividad_doc (actividad_id, documento_id, actividad_doc_ent)
VALUES
(1, 1, 'true'),
(2, 2, 'false'),
(3, 3, 'false'),
(4, 4, 'false'),
(5, 5, 'false');

INSERT INTO actividad_modalidad (actividad_id, modalidad_id, mod_act_duracion, mod_act_costo, mod_act_pasos, mod_act_nota, mod_act_contacto)
VALUES 
(
1,
1,
60,
3600,
'Esta actividad solo se puede realizar en las oficinas del Registro Civil, para esto debe:
	- Dirijirse a la sección de cédulas de identidad de cualquier oficina del Registro Civil.
 	- Explique el motivo de su visita e indique los datos personales que le soliciten.
	- Cancele el precio de la cédula de identidad.
	- Imprima sus huellas digitales (Solo cuando esta solicitando por primera vez).
	- Tomese la fotografía y reciba el comprobante.
	- Espere 8 días hábiles y con el comprobante retire su cédula en la misma oficina donde solicito.',
'Puede revisar el estado de su cédula en: ',
'https://portal.sidiv.registrocivil.cl/usuarios-portal/pages/DocumentStatus.xhtml'),
(
2,
1,
30,
210,
'Actividad bajo su modalidad En Oficina: 
	- Diríjase a la sección de certificados de cualquier oficina del Registro Civil.
	- Solicite en el mesón el certificado de nacimiento indicando el propósito para que lo necesita e indique los datos personales que le soliciten.
	- Cancele el precio del certificado y reciba.',
NULL,
NULL),
(
2,
2,
2,
0,
'Actividad bajo su modalidad En Linea: 
	- Diríjase a la sección de "Certificados en Linea -> Certificados gratuitos" de la página web del registro civil.
	- Lea los términos y requisitos para realizar este proceso y haga click sobre "Obtener Certificado".
	- Elija Certificado de Nacimiento e ingrese su rut para acceder.
	- Ingrese los datos personales que le solicitan y un correo donde recibira el certificado en formato pdf
	- Finalize y tras esto será enviado dentro de los próximos minutos',
'Acceda a la obtención del certificado en:',
'https://www.registrocivil.cl/OficinaInternet/jsp/certificadoGratuito/CertificadosGratuitosExplicacion.jsp'),
(
3,
1,
30,
710,
'Actividad bajo su modalidad En Oficina: 
	- Diríjase a la sección de certificados de cualquier oficina del Registro Civil.
	- Solicite en el mesón el certificado de nacimiento indicandolo que lo necesita para todo trámite e indique los datos personales que le soliciten.
	- Cancele el precio del certificado y reciba.',
NULL,
NULL),
(
3,
2,
2,
710,
'Actividad bajo su modalidad En Linea: 
	- Diríjase a la sección de "Certificados en Linea -> Nacimiento" de la página web del registro civil.
	- Lea los términos y requisitos para realizar este proceso y haga click sobre "Iniciar Compra".
	- Elija tipo de certificado para todo trámite e ingrese su rut para acceder.
	- Ingrese los datos personales que le solicitan y un correo donde recibira el certificado en formato pdf
	- Finalize y tras esto será enviado dentro de los próximos minutos',
'Acceda a la obtención del certificado en:',
'https://www.registrocivil.cl/OficinaInternet/html/nacimiento_explicacion.jsp'),
(
4,
1,
30,
4900,
'Esta actividad solo se puede realizar en alguna Notaría Certificada por ambos padres o aquel que no viaja con el menor: 
	- Cree un permiso de autorización de viaje para menores de edad con sus datos personales y del menor
	- Diríjase a cualquier notaría y certifique su documento bajo notario
	- Cancele los costos asociados',
NULL,
NULL),
(
5,
1,
30,
48900,
'Esta actividad solo se puede realizar en las oficinas del Registro Civil, para esto debe:
	- Dirijirse al módulo de atención de la oficina del Registro Civil.
 	- Explique el motivo de su visita y entregue los documentos requeridos.
	- Cancele el precio del pasaporte.',
'Puede revisar el estado de su pasaporte en: ',
'https://portal.sidiv.registrocivil.cl/usuarios-portal/pages/DocumentStatus.xhtml');


INSERT INTO oficina (comuna_id, organismo_id, oficina_nombre, oficina_direccion, oficina_tel_fijo, oficina_tel_celu, oficina_horario)
VALUES
(
104, 
1, 
'Oficina Maipú',
'Av. 5 de Abril 449',
'600 370 2000',
NULL,
'Lunes a Viernes 08:30 a 14:00 horas'
),
(
105, 
1, 
'Oficina Ñuñoa',
'Dublé Almeyda 3143',
'600 370 2000',
NULL,
'Lunes a Viernes 08:30 a 14:00 horas'
),
(
87, 
1, 
'Oficina Cerrillos',
'Las Hortensias 400 Camino a Melipilla',
'600 370 2000',
NULL,
'Lunes a Viernes 08:30 a 14:00 horas'
),
(
87, 
1, 
'Notaria Cerrillos',
'Av.Pedro Aguirre Cerda 6087 P.2',
'(2) 323 1209',
NULL,
'Lunes a Viernes 08:30 a 14:00 horas'
),
(
104, 
1, 
'CAMMAS MONTES MANUEL C.P.',
'Monumento 2079',
'(2) 532 4424',
NULL,
'Lunes a Viernes 08:30 a 14:00 horas'
);

INSERT INTO actividad_oficina (actividad_id, oficina_id)
VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 4),
(4, 5),
(5, 1),
(5, 2),
(5, 3);

INSERT INTO categorias_tramite (tramite_id, categoria_id)
VALUES
(1, 7),
(2, 7),
(3, 7),
(3, 3);

INSERT INTO documento_tramite (tramite_id, documento_id)
VALUES
(1, 1);

INSERT INTO edicion (usuario_id, tramite_id, edicion_fecha, edicion_tipo, edicion_descripcion)
VALUES
(1, 1, NOW(), 'C', 'Trámite recien creado');

INSERT INTO tramite_modalidad (tramite_id, modalidad_id)
VALUES
(1,1);

INSERT INTO etapa (actividad_id, modalidad_act_id, actividad_sig_id, modalidad_act_sig_id, tramite_id, modalidad_tra_id, etapa_nro, etapa_espera)
VALUES
(1, 1, 5, 1, 1, 1, 1, NULL),
(5, 1, 0, 0, 1, 1, 2, NULL);


INSERT INTO ficha_tecnica (tramite_id, ficha_numero_visitas, ficha_fecha_creacion, ficha_fecha_actualizado, ficha_estado, ficha_comentario_estado, ficha_destacado)
VALUES
(1, 20, NOW(), NOW(), 'A', NULL, 'true'),
(2, 24, NOW(), NOW(), 'EC', NULL, 'false'),
(3, 2, NOW(), NOW(), 'EC', NULL, 'false');

INSERT INTO formato_documento (documento_id, formato_doc_nombre, formato_doc_costo, formato_doc_archivo)
VALUES
(1, 'F', 3600, NULL),
(2, 'F', 210, NULL),
(2, 'V', 0, NULL),
(3, 'F', 710, NULL),
(3, 'V', 710, NULL),
(4, 'F', 4900, NULL),
(5, 'F', 48900, NULL),
(6, 'F', 60720, NULL);

INSERT INTO requisitos_tramite (tramite_id, requisito_id)
VALUES
(1,1),
(1,2),
(1,5);

INSERT INTO palabra_ficha_tramite (tramite_id, palabra_id)
VALUES
(1,1),
(1,2),
(1,4);

INSERT INTO marco_legal_tramite (tramite_id, ley_id)
VALUES
(1, 1);

INSERT INTO marcado (tramite_id, usuario_id, marcado_fecha, marcado_notificacion)
VALUES
(1, 3, NOW(), 'true');

INSERT INTO tiempo_usuario (usuario_id, actividad_id, modalidad_id, tiempo_usuario_minutos)
VALUES
(3, 1, 1, 60);

INSERT INTO valoracion (usuario_id, oficina_id, valoracion_numero)
VALUES
(3, 1, 3);

/*
INSERT INTO relacionados(tramite_id, traminet_rel_id, relacionados_info)
VALUES
(1, 2, NULL);

INSERT INTO orden_trabajo (usuario_envia_id, usuario_recibe_id, orden_asunto, orden_descripcion, orden_fecha, orden_fecha_entrega, orden_estado)
VALUES
(
1, 
2, 
'Crear trámite de cédula', 
'Debes crear el trámite de obtención y renovación de cédula de indentidad, es necesario tener todos los datos.', 
'2014/11/27', 
'2014/12/05', 
NULL
);
INSERT INTO solicitud (usuario_envia_id, usuario_recibe_id, solicitud_tipo, solicitud_mensaje, solicitud_fecha, solicitud_archivada)
VALUES
(
3,
1,
'TR',
'Estimado Administrador, deseo solicitar el trámite de cédula de identidad, agradesco de ante mano mucha gracias.',
'2014-11-27',
'true'
);
*/
