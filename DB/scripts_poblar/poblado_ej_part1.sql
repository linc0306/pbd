﻿INSERT INTO organismo (organismo_nombre, organismo_descripcion, organismo_tel_fijo, organismo_tel_celu, organismo_pagina_web, organismo_logo)
VALUES
(
'Servicio de Registro Civil e Identificación',
'Es el servicio público chileno encargado de llevar los registros relativos al estado civil de las personas naturales y otros que la ley le encomienda.Entre sus tareas se encuentra el otorgamiento de certificados de nacimientos, matrimonios, defunciones, antecedentes, cédula de identidad, pasaporte, patentes de vehículos y posesiones efectivas de herencia intestadas.',
'2 26552101',
'600 370 2000',
'http://www.srcei.cl/',
NULL
),
(
'Notarias',
'Oficinas de notarios donde realizar diversos trámites extrajudiciales. El notario es un funcionario público del Estado que debe proporcionar a los ciudadanos la seguridad jurídica que promete la Constitución en su artículo 9º en el ámbito del tráfico jurídico extrajudicial. Al tiempo es un profesional del Derecho que ejerce en régimen de competencia. Esta doble cualidad  garantiza su independencia.',
NULL,
NULL,
'http://www.notariosyconservadores.cl/',
NULL
);

INSERT INTO tramite (organismo_id, tramite_titulo, tramite_descripcion, tramite_dur_anual, tramite_inicio, tramite_fin, tramite_resultado) 
VALUES
(
1,
'Pasaporte para mayores de edad (obtención y renovación)',
'Permite obtener la libreta de pasaporte para chilenos mayores de edad, que se encuentren en territorio nacional, que necesiten viajar al extranjero.

Nota: Información solo para obtener pasaporte para mayores de edad en Chile, para otros casos ver Trámites Relacionados', 
'true',
NULL,
NULL,
'Como resultado del trámite, habrá solicitado la obtención o renovación del pasaporte, el que podrá retirar en un plazo máximo de seis días hábiles. El documento obtenido tendrá 5 año de vigencia'
),
(
1,
'Pasaporte para menores de edad (obtención y renovación)',
'Permite obtener la libreta de pasaporte para chilenos menores de edad, que se encuentren en territorio nacional, que necesiten viajar al extranjero. Solo puede solicitarlo ambos padres, quien tenga tuición del menor o solo uno de los padres con permiso del otro.

Nota: Información solo para obtener pasaporte para menores de edad en Chile, para otros casos ver Trámites Relacionados',
'true',
NULL,
NULL,
'Como resultado del trámite, habrá solicitado la obtención o renovación del pasaporte, el que podrá retirar en un plazo máximo de seis días hábiles. El documento obtenido tendrá 5 año de vigencia'
),
(
1,
'Pasaporte para chilenos en el extranjero',
'Permite que chilenos en el exterior puedan solicitar un pasaporte de lectura mecánica en caso de pérdida, vencimiento del documento o cuando la persona no posee ningún tipo de identificación chilena.

Nota: Información para chilenos que necesiten pedir un pasaporte fuera de Chile, para otros casos ver Trámites Relacionados',
'true',
NULL,
NULL,
'Como resultado del trámite, habrá solicitado el pasaporte de lectura mecánica.'
);

INSERT INTO actividad (actividad_titulo, actividad_descripcion)
VALUES
(
'Obtención de la cédula de indentidad', 
'Si es que no posee cédula de identidad, se le extravió o no se encuentra vigente, realizar los siguientes pasos para su obtención.'
),
(
'Obtención de certificado de nacimiento para mátricula o asignación familiar', 
'Obtener certificado de nacimiento que muestra fecha de nacimiento y vérifica los padres de una persona, para usuarse solo en mátricula en un recinto educacional o para trámites de asignación familiar'
),
(
'Obtención de certificado de nacimiento para todo trámite', 
'Obtener certificado de nacimiento que muestra fecha de nacimiento y vérifica los padres de una persona, para usuarse en cualquier trámite'
),
(
'Obtención de autorización a menores para viajar (Si viaja solo)',
'Consiste en obtener la legalización de un documento “autorización de viaje”, dada por uno o ambos padres, que permite viajar fuera del país a un menor de 18 años.'
),

('Obtención del pasaporte',
'Obtener y pagar el pasaporte en el Registro Civil');

INSERT INTO modalidad (modalidad_titulo, modalidad_descripcion)
VALUES
(
'En Oficina', 
'Modalidad donde la presencia en la oficina es obligatoria'
),
(
'En Linea', 
'Modalidad donde todas las actividades posibles se hacen por Internet'
),
(
'Por teléfono', 
'Modalidad donde todas las actividades posibles se hacen por teléfono e Internet'
),
(
'Por correo', 
'Modalidad donde todas las actividades posibles se hacen por correo, teléfono e Internet'
);


INSERT INTO categoria (categoria_titulo, categoria_descripcion)
VALUES
(
'Educación', 
'Trámites asociados a cada etapa educativa sea para menores o adultos'
),
(
'Empresas',
'Trámites orientados a las empresas'
),
(
'Extranjeros',
'Trámites asociados a las relaciones exteriores, sea de extranjeros en Chile o chilenos en el extranjeros'
),
(
'Familia',
'Trámites asociados a las actividades familiares'
),
(
'Legal',
'Trámites de carácter judicial y legal'
),
(
'Beneficios',
'Trámites asociados a los procesos de previsión, jubilación y procesos estatales'
),
(
'Identificación',
'Trámites asociados a los procesos de identificación de personas'
),
(
'Transporte',
'Trámites asociados al permiso y uso de medios de transporte'
),
(
'Vivienda',
'Trámites asociados a la ayuda, obtención y beneficios para viviendas'
),
(
'Trabajo',
'Trámites asociados a cualquier trabajo dentro del territorio nacional'
),
(
'Salud',
'Trámites asociados a la salud tanto pública como privada'
),
(
'Otros',
'Trámites que no poseen categoría específica'
);

INSERT INTO palabra_clave (palabra_nombre)
VALUES
('pasaporte'),('viajar'),('extranjeros'),('visa');

INSERT INTO requisito (requisito_titulo, requisito_descripcion)
VALUES
('Ser mayor de edad', 'Persona mayor de 18 años.'),
('Ser chileno', 'Persona con nacionalidad chilena'),
('Ser menor de edad', 'Persona menor de 18 años'),
('Ser padre o tutor del menor de edad', 'Padre, Madre o tutor legal del menor en cuestión'),
('No tener impedimentos legales para salir del pais', 'No tener alguna acción judicial en su contra pendiente o activa');

INSERT INTO documento (documento_titulo, documento_tipo, documento_descripcion)
VALUES
(
'Cédula de indentidad', 
'Cédula',
'Es un documento público que contiene datos de identificación personal, emitido por un empleado público con autoridad competente para permitir la identificación personal e inequívoca de los ciudadanos.'
),
('Certificado de Nacimiento para matriculas o asignación familiar',
'Certificado',
'Certificado que muestra datos personales de una persona nacida e inscrita en territorio nacional, para ser usada en la mátricula de una persona en una institución educativa o para trámites de asignación familiar.
'),
('Certificado de Nacimiento para todo trámite',
'Certificado',
'Certificado que muestra datos personales de una persona nacida e inscrita en territorio nacional, para ser usada en cualquier tipo de trámite que lo solicite.
'),
(
'Poder de autorización de viajes para menores de edad', 
'Declaración jurada',
'Poder que se solicita en alguna notaría, donde ambos padres del menor o el tutor por orden judicial, entregan permiso para que el menor pueda salir del pais.'
),
('Pasaporte de 32 páginas',
'Libreta',
'El pasaporte es una libreta que corresponde al documento oficial para viajar al extranjero, ya que contiene toda la actividad oficial 
(incluidas las visas) y sirve como identificación de un chileno fuera del país. Este pasaporte es el clásico ,posee solo 32 páginas y tiene una vigencia de 5 años.
'),
('Pasaporte de 64 páginas',
'Libreta',
'El pasaporte es una libreta que corresponde al documento oficial para viajar al extranjero, ya que contiene toda la actividad oficial 
(incluidas las visas) y sirve como identificación de un chileno fuera del país. Este pasaporte es de caracter ejecutico, posee 64 páginas y tiene una vigencia de 5 años.
');

INSERT INTO ley (ley_titulo, ley_descripcion)
VALUES
('Decreto 1010', 'Aprueba Reglamento de Pasaportes Ordinarios y de Documentos de Viaje y Títulos de Viaje para extranjeros');

INSERT INTO usuario (comuna_id, usuario_nombre, usuario_apellido_pat, usuario_apellido_mat, usuario_correo, usuario_fecha_nacimiento, usuario_password, usuario_fecha_registro, usuario_tipo, usuario_estado)
VALUES
(104, 'José Ignacio', 'Cortés', 'Tapia', 'jose.cortes@usach.cl', NULL, 'traminet', NOW(), 'AI', 'true' ),
(118, 'Gonzalo Javier', 'Martinez', 'Ramirez', 'gonzalo.martinez@usach.cl', NULL, 'traminet', NOW(), 'CO', 'true' );


INSERT INTO actividad(
            actividad_id, actividad_titulo, actividad_descripcion)
    VALUES (0, NULL, NULL);

INSERT INTO modalidad(
            modalidad_id, modalidad_titulo, modalidad_descripcion)
    VALUES (0, NULL, NULL);

INSERT INTO actividad_modalidad (actividad_id, modalidad_id, mod_act_duracion, mod_act_costo, mod_act_pasos, mod_act_nota, mod_act_contacto)
VALUES (0, 0, NULL, NULL, NULL, NULL, NULL);

