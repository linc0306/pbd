CREATE OR REPLACE FUNCTION SP_TRG_NOTIF_MOD()
	RETURNS TRIGGER
	AS $$	

	if TD["event"] == "INSERT":
		id_tramite = TD["new"]["tramite_id"]
		id_modalidad = TD["new"]["modalidad_id"]
		accion = "agregado"
		accion2 = "al"

	elif TD["event"] == "DELETE":
		id_tramite = TD["old"]["tramite_id"]
		id_modalidad = TD["old"]["modalidad_id"]
		accion = "eliminado"
		accion2 = "del"
	
	#Crear notificacion, estos se crean solo si el tramite se encuentra activo cuando se hizo el cambio
	
	#Obtener nombre y estado del tramite editado
	qry_info_tra = plpy.prepare("SELECT TRAMITE_TITULO, FICHA_ESTADO FROM TRAMITE, FICHA_TECNICA WHERE TRAMITE.TRAMITE_ID = FICHA_TECNICA.TRAMITE_ID AND TRAMITE.TRAMITE_ID = $1", ["INTEGER"])
	info_tra = plpy.execute(qry_info_tra, [id_tramite], 1)

	if info_tra[0]["ficha_estado"] == "A":#activo

		#Obtener nombre de la modalidad
		qry_nombre_mod = plpy.prepare("SELECT MODALIDAD_TITULO FROM MODALIDAD WHERE MODALIDAD_ID = $1", ["INTEGER"])
		nombre_mod = plpy.execute(qry_nombre_mod, [id_modalidad])

		#mensaje de la notif
		titulo_not = "TRAMINET te informa: Cambios de modalidades en: '" + info_tra[0]["tramite_titulo"]+"'"
		descripcion_not = "Se ha " + accion + " la modalidad :'"+ nombre_mod[0]["modalidad_titulo"] + "' "+ accion2 + " tramite: '"+ info_tra[0]["tramite_titulo"]+"'"
		
		qry_crearNot = plpy.prepare("INSERT INTO NOTIFICACION(NOTIFICACION_TITULO, NOTIFICACION_DESCRIPCION) VALUES ($1, $2)", ["text", "text"])
		crearNot = plpy.execute(qry_crearNot, [titulo_not, descripcion_not])

	#mandar a enviar la notificacion, IDEA: Crear un triggers en notificacion (??no tengo el id:tramite)	

	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_NOTIF_MOD ON TRAMITE_MODALIDAD;

CREATE TRIGGER TRG_NOTIF_MOD AFTER INSERT OR DELETE
ON TRAMITE_MODALIDAD FOR EACH ROW
EXECUTE PROCEDURE SP_TRG_NOTIF_MOD();
