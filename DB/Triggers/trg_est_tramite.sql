--Trigger agregar edicion de cambio de estado de un trámite

CREATE OR REPLACE FUNCTION SP_TRG_EST_TRAMITE() 
	RETURNS TRIGGER
	AS $$	
		
	estado_antiguo = TD["old"]["ficha_estado"]
	estado_actual = TD["new"]["ficha_estado"]

	if estado_antiguo != estado_actual:

		id_tram = TD["new"]["tramite_id"]
		tipo_edicion =  estado_actual;

		if estado_actual == "EC":
			#Algo no salio bien
			ejecuto = 0

		elif estado_actual == "A":
			comentario_edicion = "Tramite activado para su visualización"
			ejecuto = 1

		elif estado_actual == "E":
			comentario_edicion = "Tramite eliminado, solo administrador puede visualizar"
			ejecuto = 1

		elif estado_actual == "B":
			comentario_edicion = TD["new"]["ficha_comentario_estado"]
			ejecuto = 1

		elif estado_actual == "R":
			#No cambiar edicion
			ejecuto = 0
		else:
			#Algo salio muy mal
			ejecuto = 0

		if ejecuto == 1:
			#agregar edicion
			qry_crearEdicion = plpy.prepare("INSERT INTO EDICION(USUARIO_ID, TRAMITE_ID, EDICION_FECHA, EDICION_TIPO, EDICION_DESCRIPCION) VALUES($1, $2, NOW(), $3, $4)", ["INTEGER", "INTEGER", "text", "text"])

			edicion_creada = plpy.execute(qry_crearEdicion, [1, id_tram, tipo_edicion, comentario_edicion]) #Ver como añadir id del usuario
		
	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_UPD_FICHA ON FICHA_TECNICA;

CREATE TRIGGER TRG_UPD_FICHA AFTER UPDATE 
	ON FICHA_TECNICA FOR EACH ROW
	EXECUTE PROCEDURE SP_TRG_EST_TRAMITE();
	

