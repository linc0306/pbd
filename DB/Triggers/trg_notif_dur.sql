CREATE OR REPLACE FUNCTION SP_TRG_NOTIF_DUR()
	RETURNS TRIGGER
	AS $$	
	
	ejecuto = 0

	id_tramite = TD["new"]["tramite_id"]
	

	if TD["old"]["tramite_dur_anual"] != TD["new"]["tramite_dur_anual"]:
		ejecuto = 1
		if TD["new"]["tramite_dur_anual"] == True:
			mensaje  = "Ahora puedes realizarlo en cualquier fecha del año."
		else:
			mensaje  = "Ahora solo puedes realizarlo desde el " + TD["new"]["tramite_inicio"] + " hasta el " + TD["new"]["tramite_fin"]+"."

	else:

		if TD["old"]["tramite_inicio"] != TD["new"]["tramite_inicio"]:
			ejecuto = 1

		if TD["old"]["tramite_fin"] != TD["new"]["tramite_fin"]:
			ejecuto = 1

		if ejecuto == 1:
			mensaje = "Ahora realizalo desde el "+ TD["new"]["tramite_inicio"] + " hasta el " + TD["new"]["tramite_fin"]+"."
	

	if ejecuto == 1:

		#Crear notificacion, estos se crean solo si el tramite se encuentra activo cuando se hizo el cambio
		
		#Obtener nombre y estado del tramite editado
		qry_info_tra = plpy.prepare("SELECT TRAMITE_TITULO, FICHA_ESTADO FROM TRAMITE, FICHA_TECNICA WHERE TRAMITE.TRAMITE_ID = FICHA_TECNICA.TRAMITE_ID AND TRAMITE.TRAMITE_ID = $1", ["INTEGER"])
		info_tra = plpy.execute(qry_info_tra, [id_tramite], 1)

		if info_tra[0]["ficha_estado"] == "A":#activo
			
			#mensaje de la notif
			titulo_not = "TRAMINET te informa: Cambios de plazos en: '" + info_tra[0]["tramite_titulo"]+"'"
			descripcion_not = "Los plazos del tramite '" + info_tra[0]["tramite_titulo"] + "' han cambiado. " + mensaje
			
			qry_crearNot = plpy.prepare("INSERT INTO NOTIFICACION(NOTIFICACION_TITULO, NOTIFICACION_DESCRIPCION) VALUES ($1, $2)", ["text", "text"])
			crearNot = plpy.execute(qry_crearNot, [titulo_not, descripcion_not])

		#mandar a enviar la notificacion, IDEA: Crear un triggers en notificacion (??no tengo el id:tramite)	

	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_NOTIF_DUR ON TRAMITE;

CREATE TRIGGER TRG_NOTIF_DUR AFTER UPDATE 
ON TRAMITE FOR EACH ROW
EXECUTE PROCEDURE SP_TRG_NOTIF_DUR();