CREATE OR REPLACE FUNCTION SP_TRG_NOTIF_ORDEN()
	RETURNS TRIGGER
	AS $$	

	titulo_not = "Orden de trabajo: "+ TD["new"]["orden_asunto"]+"."
	descripcion_not = "Se ha recibido una nueva orden de trabajo a cumplirse antes del " + TD["new"]["orden_fecha_entrega"] + ". Para mas detalle entre a su cuenta de TRAMINET."

	qry_crearNot = plpy.prepare("INSERT INTO NOTIFICACION(NOTIFICACION_TITULO, NOTIFICACION_DESCRIPCION) VALUES ($1, $2)", ["text", "text"])
	crearNot = plpy.execute(qry_crearNot, [titulo_not, descripcion_not])

	#mandar a enviar la notificacion, IDEA: Crear un triggers en notificacion (??no tengo el id:tramite)	

	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_NOTIF_ORDEN ON ORDEN_TRABAJO;

CREATE TRIGGER TRG_NOTIF_ORDEN AFTER INSERT 
ON ORDEN_TRABAJO FOR EACH ROW
EXECUTE PROCEDURE SP_TRG_NOTIF_ORDEN();