--Trigger actualiza valor de costo de una act_mod tras cambiar el precio de un documento bajo cierto formato

CREATE OR REPLACE FUNCTION SP_TRG_FORMATO_DOC() 
	RETURNS TRIGGER
	AS $$	
		
	costo_antiguo = TD["old"]["formato_doc_costo"]
	costo_actual = TD["new"]["formato_doc_costo"]
	dif_costo = costo_actual - costo_antiguo

	if dif_costo != 0:

		qry_contarFormatos = plpy.prepare("SELECT COUNT(*) AS CANT_FORM FROM FORMATO_DOCUMENTO WHERE DOCUMENTO_ID = $1",["INTEGER"])
		cantFormatos = plpy.execute(qry_contarFormatos, [ TD["new"]["documento_id"] ])

		qry_get_actMod = plpy.prepare("SELECT ACTIVIDAD_DOC.ACTIVIDAD_ID, MODALIDAD.MODALIDAD_ID, MODALIDAD_TITULO FROM ACTIVIDAD_DOC, ACTIVIDAD_MODALIDAD, MODALIDAD WHERE ACTIVIDAD_DOC.ACTIVIDAD_ID = ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID AND MODALIDAD.MODALIDAD_ID = ACTIVIDAD_MODALIDAD.MODALIDAD_ID AND DOCUMENTO_ID = $1",["INTEGER"])
		
		act_mod = plpy.execute(qry_get_actMod, [TD["new"]["documento_id"]])

		qry_cambiarCosto = plpy.prepare("UPDATE ACTIVIDAD_MODALIDAD SET MOD_ACT_COSTO = MOD_ACT_COSTO + $1 WHERE ACTIVIDAD_ID = $2 AND MODALIDAD_ID = $3 ", ["INTEGER", "INTEGER", "INTEGER"])


		for fila_actMod in act_mod:

			if cantFormatos[0]["cant_form"] == 1:
								
					cambiarCosto = plpy.execute(qry_cambiarCosto, [dif_costo, fila_actMod["actividad_id"], fila_actMod["modalidad_id"] ])

			elif cantFormatos[0]["cant_form"] == 2:

				if TD["new"]["formato_doc_nombre"] == "F":

					if fila_actMod["modalidad_titulo"] == "En Oficina":
						
						cambiarCosto = plpy.execute(qry_cambiarCosto, [dif_costo, fila_actMod["actividad_id"], fila_actMod["modalidad_id"] ])

				elif TD["new"]["formato_doc_nombre"] == "V":

					if fila_actMod["modalidad_titulo"] != "En Oficina":

						cambiarCosto = plpy.execute(qry_cambiarCosto, [dif_costo, fila_actMod["actividad_id"], fila_actMod["modalidad_id"] ])
		
	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_FORMATO_DOC ON FORMATO_DOCUMENTO;

CREATE TRIGGER TRG_FORMATO_DOC AFTER UPDATE 
	ON FORMATO_DOCUMENTO FOR EACH ROW
	EXECUTE PROCEDURE SP_TRG_FORMATO_DOC();