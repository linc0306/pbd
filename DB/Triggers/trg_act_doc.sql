	--Trigger agregar edicion de cambio de estado de un trámite

CREATE OR REPLACE FUNCTION SP_TRG_ACT_DOC() 
	RETURNS TRIGGER
	AS $$	
		
	accion = TD["event"]
	sum_rest = 0;

	if accion == "INSERT":
		id_actividad = TD["new"]["actividad_id"]
		id_documento = TD["new"]["documento_id"]
		entregable = TD["new"]["actividad_doc_ent"]
		sum_rest = 1

	elif accion == "DELETE":
		id_actividad = TD["old"]["actividad_id"]
		id_documento = TD["old"]["documento_id"]
		entregable = TD["old"]["actividad_doc_ent"]
		sum_rest = -1

	#Actualizar Costo

	qry_getFormatos = plpy.prepare("SELECT FORMATO_DOC_ID, FORMATO_DOC_NOMBRE, FORMATO_DOC_COSTO FROM FORMATO_DOCUMENTO WHERE DOCUMENTO_ID = $1",["INTEGER"])

	formatos_doc = plpy.execute(qry_getFormatos, [id_documento])

	qry_getModAct = plpy.prepare("SELECT MODALIDAD.MODALIDAD_ID, MODALIDAD_TITULO FROM ACTIVIDAD_MODALIDAD, MODALIDAD WHERE ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID = $1 AND ACTIVIDAD_MODALIDAD.MODALIDAD_ID = MODALIDAD.MODALIDAD_ID", ["INTEGER"])

	modalidad_act = plpy.execute(qry_getModAct, [id_actividad])

	qry_cambiarCosto = plpy.prepare("UPDATE ACTIVIDAD_MODALIDAD SET MOD_ACT_COSTO = MOD_ACT_COSTO + $1 WHERE ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID = $2 AND ACTIVIDAD_MODALIDAD.MODALIDAD_ID = $3", ["INTEGER", "INTEGER", "INTEGER"])

	for fila_mod in modalidad_act:		

		cant_formatos = formatos_doc.nrows()

		for fila_doc in formatos_doc:

			costo_doc = fila_doc["formato_doc_costo"]
			costo_doc = costo_doc * sum_rest

			if(cant_formatos == 1):				
				
				actualizado_costo = plpy.execute(qry_cambiarCosto, [costo_doc, id_actividad, fila_mod["modalidad_id"]])

			elif(cant_formatos == 2):

				if fila_doc["formato_doc_nombre"] == "F" and fila_mod["modalidad_titulo"] == "En Oficina":

					actualizado_costo = plpy.execute(qry_cambiarCosto, [costo_doc, id_actividad, fila_mod["modalidad_id"]])

				elif fila_doc["formato_doc_nombre"] == "V" and fila_mod["modalidad_titulo"] != "En Oficina":

					actualizado_costo = plpy.execute(qry_cambiarCosto, [costo_doc, id_actividad, fila_mod["modalidad_id"]])

				#else:
					#hay problemas, algo no salio bien
			#else:
				#hay problemas, algo no salio bien

	#Obtener documentos finales de un tramite
	if entregable == True:

		qry_actividad_tram = plpy.prepare("SELECT TRAMITE.TRAMITE_ID, ACTIVIDAD_ID FROM TRAMITE, ETAPA WHERE TRAMITE.TRAMITE_ID = ETAPA.TRAMITE_ID AND ETAPA.ACTIVIDAD_ID = $1", ["INTEGER"])
		actividad_tram = plpy.execute(qry_actividad_tram, [id_actividad])

		if accion == "INSERT":

			qry_ins_doc_tram = plpy.prepare("INSERT INTO DOCUMENTO_TRAMITE(TRAMITE_ID, DOCUMENTO_ID) VALUES($1, $2)",["INTEGER","INTEGER"])

			for fila_tram in actividad_tram:

				ins_doc_tram = plpy.execute(qry_ins_doc_tram, [fila_tram["tramite_id"], id_documento])

		elif accion == "DELETE":

			qry_del_doc_tram = plpy.prepare("DELETE FROM DOCUMENTO_TRAMITE WHERE TRAMITE_ID = $1 AND DOCUMENTO_ID = $2",["INTEGER","INTEGER"])

			for fila_tram in actividad_tram:

				del_doc_tram = plpy.execute(qry_del_doc_tram, [fila_tram["tramite_id"], id_documento])


	$$ LANGUAGE plpython2u;
DROP TRIGGER IF EXISTS TRG_ACT_DOC ON ACTIVIDAD_DOC;

CREATE TRIGGER TRG_ACT_DOC AFTER INSERT OR DELETE 
	ON ACTIVIDAD_DOC FOR EACH ROW
	EXECUTE PROCEDURE SP_TRG_ACT_DOC();