CREATE OR REPLACE FUNCTION SP_TRG_NOTIF_SOL()
	RETURNS TRIGGER
	AS $$		

	if TD["new"]["solicitud_tipo"] == "T":
		tipo = "Tramite"

	elif TD["new"]["solicitud_tipo"] == "O":
		tipo = "Organismo"

	elif TD["new"]["solicitud_tipo"] == "D":
		tipo = "Documento"

	titulo_not = "Solicitud de informacion sobre "+ tipo+"."
	descripcion_not = "Se ha recibido una nueva solicitud de informacion sobre un "+tipo+". Para mas detalle entre a su cuenta de TRAMINET."

	qry_crearNot = plpy.prepare("INSERT INTO NOTIFICACION(NOTIFICACION_TITULO, NOTIFICACION_DESCRIPCION) VALUES ($1, $2)", ["text", "text"])
	crearNot = plpy.execute(qry_crearNot, [titulo_not, descripcion_not])

	#mandar a enviar la notificacion, IDEA: Crear un triggers en notificacion (??no tengo el id:tramite)	

	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_NOTIF_SOL ON SOLICITUD;

CREATE TRIGGER TRG_NOTIF_SOL AFTER INSERT 
ON SOLICITUD FOR EACH ROW
EXECUTE PROCEDURE SP_TRG_NOTIF_SOL();