--Trigger crear ficha de un tramite

CREATE OR REPLACE FUNCTION SP_TRG_INS_TRAMITE() 
	RETURNS TRIGGER
	AS $$	
		
	id_tram = TD["new"]["tramite_id"]

	#creacion de ficha tecnica		
    	qry_crearFicha = plpy.prepare("INSERT INTO FICHA_TECNICA (TRAMITE_ID, FICHA_NUMERO_VISITAS, FICHA_FECHA_CREACION, FICHA_FECHA_ACTUALIZADO, FICHA_ESTADO, FICHA_COMENTARIO_ESTADO, FICHA_DESTACADO) VALUES ($1, $2, NOW(), NOW(), $3, NULL , $4)", ["INTEGER", "INTEGER", "text", "boolean"])

    	ficha_creada = plpy.execute(qry_crearFicha, [ id_tram, 0,"EC", 0])

    	#agregar edicion
    	qry_crearEdicion = plpy.prepare("INSERT INTO EDICION(USUARIO_ID, TRAMITE_ID, EDICION_FECHA, EDICION_TIPO, EDICION_DESCRIPCION) VALUES($1, $2, NOW(), 'C', 'Trámite recien creado')", ["INTEGER", "INTEGER"])

    	edicion_creada = plpy.execute(qry_crearEdicion, [1, id_tram]) #Ver como añadir id del usuario
		
	$$ LANGUAGE plpython2u;

DROP TRIGGER IF EXISTS TRG_INS_TRAMITE ON TRAMITE;

CREATE TRIGGER TRG_INS_TRAMITE AFTER INSERT 
	ON TRAMITE FOR EACH ROW
	EXECUTE PROCEDURE SP_TRG_INS_TRAMITE();
	

