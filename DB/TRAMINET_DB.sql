/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     04-12-2014 16:56:21                          */
/*==============================================================*/


drop index if exists ACTIVIDAD_PK;

drop table if exists ACTIVIDAD;

drop index if exists RELATIONSHIP_23_FK;

drop index if exists RELATIONSHIP_22_FK;

drop index if exists ACTIVIDAD_DOC_PK;

drop table if exists ACTIVIDAD_DOC;

drop index if exists RELATIONSHIP_51_FK;

drop index if exists RELATIONSHIP_50_FK;

drop index if exists ACTIVIDAD_MODALIDAD_PK;

drop table if exists ACTIVIDAD_MODALIDAD;

drop index if exists RELATIONSHIP_53_FK;

drop index if exists RELATIONSHIP_52_FK;

drop index if exists ACTIVIDAD_OFICINA_PK;

drop table if exists ACTIVIDAD_OFICINA;

drop index if exists CATEGORIA_PK;

drop table if exists CATEGORIA;

drop index if exists RELATIONSHIP_9_FK;

drop index if exists RELATIONSHIP_8_FK;

drop index if exists CATEGORIAS_TRAMITE_PK;

drop table if exists CATEGORIAS_TRAMITE;

drop index if exists RELATIONSHIP_32_FK;

drop index if exists COMUNA_PK;

drop table if exists COMUNA;

drop index if exists DOCUMENTO_PK;

drop table if exists DOCUMENTO;

drop index if exists RELATIONSHIP_41_FK;

drop index if exists RELATIONSHIP_40_FK;

drop index if exists DOCUMENTO_TRAMITE_PK;

drop table if exists DOCUMENTO_TRAMITE;

drop index if exists RELATIONSHIP_46_FK;

drop index if exists RELATIONSHIP_45_FK;

drop index if exists EDICION_PK;

drop table if exists EDICION;

drop index if exists RELATIONSHIP_37_FK;

drop index if exists RELATIONSHIP_17_FK;

drop index if exists RELATIONSHIP_16_FK;

drop index if exists ETAPA_PK;

drop table if exists ETAPA;

drop index if exists FICHA_TECNICA_PK;

drop table if exists FICHA_TECNICA;

drop index if exists RELATIONSHIP_49_FK;

drop index if exists FORMATO_DOCUMENTO_PK;

drop table if exists FORMATO_DOCUMENTO;

drop index if exists LEY_PK;

drop table if exists LEY;

drop index if exists LOG_SQL_PK;

drop table if exists LOG_SQL;

drop index if exists RELATIONSHIP_27_FK;

drop index if exists RELATIONSHIP_26_FK;

drop index if exists MARCADO_PK;

drop table if exists MARCADO;

drop index if exists RELATIONSHIP_7_FK;

drop index if exists RELATIONSHIP_6_FK;

drop index if exists MARCO_LEGAL_TRAMITE_PK;

drop table if exists MARCO_LEGAL_TRAMITE;

drop index if exists MODALIDAD_PK;

drop table if exists MODALIDAD;

drop index if exists NOTIFICACION_PK;

drop table if exists NOTIFICACION;

drop index if exists RELATIONSHIP_39_FK;

drop index if exists RELATIONSHIP_25_FK;

drop index if exists OFICINA_PK;

drop table if exists OFICINA;

drop index if exists RELATIONSHIP_47_FK;

drop index if exists RELATIONSHIP_44_FK;

drop index if exists ORDEN_TRABAJO_PK;

drop table if exists ORDEN_TRABAJO;

drop index if exists ORGANISMO_PK;

drop table if exists ORGANISMO;

drop index if exists PALABRA_CLAVE_PK;

drop table if exists PALABRA_CLAVE;

drop index if exists RELATIONSHIP_36_FK;

drop index if exists RELATIONSHIP_35_FK;

drop index if exists PALABRA_FICHA_TRAMITE_PK;

drop table if exists PALABRA_FICHA_TRAMITE;

drop index if exists RELATIONSHIP_31_FK;

drop index if exists PROVINCIA_PK;

drop table if exists PROVINCIA;

drop index if exists REGION_PK;

drop table if exists REGION;

drop index if exists RELATIONSHIP_56_FK;

drop index if exists RELATIONSHIP_55_FK;

drop index if exists RELACIONADOS_PK;

drop table if exists RELACIONADOS;

drop index if exists REQUISITO_PK;

drop table if exists REQUISITO;

drop index if exists RELATIONSHIP_5_FK;

drop index if exists RELATIONSHIP_4_FK;

drop index if exists REQUISITOS_TRAMITE_PK;

drop table if exists REQUISITOS_TRAMITE;

drop index if exists RELATIONSHIP_43_FK;

drop index if exists RELATIONSHIP_42_FK;

drop index if exists SOLICITUD_PK;

drop table if exists SOLICITUD;

drop index if exists RELATIONSHIP_20_FK;

drop index if exists RELATIONSHIP_11_FK;

drop index if exists TIEMPO_USUARIO_PK;

drop table if exists TIEMPO_USUARIO;

drop index if exists RELATIONSHIP_48_FK;

drop index if exists TRAMITE_PK;

drop table if exists TRAMITE;

drop index if exists RELATIONSHIP_38_FK;

drop index if exists RELATIONSHIP_34_FK;

drop index if exists TRAMITE_MODALIDAD_PK;

drop table if exists TRAMITE_MODALIDAD;

drop index if exists RELATIONSHIP_33_FK;

drop index if exists USUARIO_PK;

drop table if exists USUARIO;

drop index if exists RELATIONSHIP_28_FK;

drop index if exists RELATIONSHIP_13_FK;

drop index if exists VALORACION_PK;

drop table if exists VALORACION;

drop domain if exists ESTADO_ORDEN;

drop domain if exists ESTADO_TRAMITE;

drop domain if exists FORMATO_DOC;

drop domain if exists NRO_VALORACION;

drop domain if exists OPERACION_LOG;

drop domain if exists TIPO_EDICION;

drop domain if exists TIPO_SOLICITUD;

drop domain if exists TIPO_USUARIO;

/*==============================================================*/
/* Domain: ESTADO_ORDEN                                         */
/*==============================================================*/
create domain ESTADO_ORDEN as VARCHAR(2);

/*==============================================================*/
/* Domain: ESTADO_TRAMITE                                       */
/*==============================================================*/
create domain ESTADO_TRAMITE as VARCHAR(2);

/*==============================================================*/
/* Domain: FORMATO_DOC                                          */
/*==============================================================*/
create domain FORMATO_DOC as VARCHAR(2);

/*==============================================================*/
/* Domain: NRO_VALORACION                                       */
/*==============================================================*/
create domain NRO_VALORACION as INT2;

/*==============================================================*/
/* Domain: OPERACION_LOG                                        */
/*==============================================================*/
create domain OPERACION_LOG as VARCHAR(8);

/*==============================================================*/
/* Domain: TIPO_EDICION                                         */
/*==============================================================*/
create domain TIPO_EDICION as VARCHAR(2);

/*==============================================================*/
/* Domain: TIPO_SOLICITUD                                       */
/*==============================================================*/
create domain TIPO_SOLICITUD as VARCHAR(2);

/*==============================================================*/
/* Domain: TIPO_USUARIO                                         */
/*==============================================================*/
create domain TIPO_USUARIO as VARCHAR(2);

/*==============================================================*/
/* Table: ACTIVIDAD                                             */
/*==============================================================*/
create table ACTIVIDAD (
   ACTIVIDAD_ID         SERIAL               not null,
   ACTIVIDAD_TITULO     VARCHAR(100)         null,
   ACTIVIDAD_DESCRIPCION VARCHAR(1000)        null,
   constraint PK_ACTIVIDAD primary key (ACTIVIDAD_ID)
);

comment on table ACTIVIDAD is
'Representa a cada una de las actividades que se realizan durante el proceso de desarrollo de trámites, como por ejemplo: En el trámite de obtención de TNE una actividad es Pagar el costo de la tarjeta.';

comment on column ACTIVIDAD.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column ACTIVIDAD.ACTIVIDAD_TITULO is
'Título con el que se nombra a una actividad.';

comment on column ACTIVIDAD.ACTIVIDAD_DESCRIPCION is
'Describe en detalle los pasos que se deben realizar durante la actividad, como por ejemplo en el caso de "Pagar costo de la tarjeta" se describen los pasos : Diríjase al mesón y deposite $XXXX en la cuenta XXXX-X, Reciba su comprobante, Almacene y no pierda el comprobante.';

/*==============================================================*/
/* Index: ACTIVIDAD_PK                                          */
/*==============================================================*/
create unique index ACTIVIDAD_PK on ACTIVIDAD (
ACTIVIDAD_ID
);

/*==============================================================*/
/* Table: ACTIVIDAD_DOC                                         */
/*==============================================================*/
create table ACTIVIDAD_DOC (
   ACTIVIDAD_ID         INT4                 not null,
   DOCUMENTO_ID         INT4                 not null,
   ACTIVIDAD_DOC_ENT    BOOL                 null,
   constraint PK_ACTIVIDAD_DOC primary key (ACTIVIDAD_ID, DOCUMENTO_ID)
);

comment on table ACTIVIDAD_DOC is
'Indica el o los documentos necesarios para la realización de una actividad.';

comment on column ACTIVIDAD_DOC.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column ACTIVIDAD_DOC.DOCUMENTO_ID is
'Identificador primario de un documento específico';

comment on column ACTIVIDAD_DOC.ACTIVIDAD_DOC_ENT is
'Indica si el documento que participa en la actividad corresponde a un documento entregable, es decir, que se necesita entregar para cumplir el trámite en general.';

/*==============================================================*/
/* Index: ACTIVIDAD_DOC_PK                                      */
/*==============================================================*/
create unique index ACTIVIDAD_DOC_PK on ACTIVIDAD_DOC (
ACTIVIDAD_ID,
DOCUMENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_22_FK on ACTIVIDAD_DOC (
ACTIVIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_23_FK on ACTIVIDAD_DOC (
DOCUMENTO_ID
);

/*==============================================================*/
/* Table: ACTIVIDAD_MODALIDAD                                   */
/*==============================================================*/
create table ACTIVIDAD_MODALIDAD (
   ACTIVIDAD_ID         INT4                 not null,
   MODALIDAD_ID         INT4                 not null,
   MOD_ACT_DURACION     INT4                 null,
   MOD_ACT_COSTO        INT4                 null,
   MOD_ACT_PASOS        VARCHAR(1500)        null,
   MOD_ACT_CONTACTO     VARCHAR(150)         null,
   MOD_ACT_NOTA         VARCHAR(150)         null,
   constraint PK_ACTIVIDAD_MODALIDAD primary key (ACTIVIDAD_ID, MODALIDAD_ID)
);

comment on table ACTIVIDAD_MODALIDAD is
'Indica las diferentes modalidades con las que se puede realizar una actividad';

comment on column ACTIVIDAD_MODALIDAD.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column ACTIVIDAD_MODALIDAD.MODALIDAD_ID is
'Identificador primario de una modalidad.';

comment on column ACTIVIDAD_MODALIDAD.MOD_ACT_DURACION is
'Cifra oficial del tiempo estimado que se gasta durante la realización de la actividad.';

comment on column ACTIVIDAD_MODALIDAD.MOD_ACT_COSTO is
'Cifra oficial del costo monetario que implica realizar la actividad.';

comment on column ACTIVIDAD_MODALIDAD.MOD_ACT_PASOS is
'Descripcion de los pasos que se deben seguir para realizar la actividad bajo cierta modalidad,';

comment on column ACTIVIDAD_MODALIDAD.MOD_ACT_CONTACTO is
'Indica el dato de contacto para realizar una actividad bajo cierta modalidad. Si es en oficina este dato es Nulo, si es en linea este dato indica la dirección web donde realizar la actividad, si es por telefono indica el número telefónico, y si es por correo indica la dirección a donde enviarlo.';

comment on column ACTIVIDAD_MODALIDAD.MOD_ACT_NOTA is
'Nota que indica que es lo que puede realizar en el dato de contacto
';

/*==============================================================*/
/* Index: ACTIVIDAD_MODALIDAD_PK                                */
/*==============================================================*/
create unique index ACTIVIDAD_MODALIDAD_PK on ACTIVIDAD_MODALIDAD (
ACTIVIDAD_ID,
MODALIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_50_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_50_FK on ACTIVIDAD_MODALIDAD (
ACTIVIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_51_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_51_FK on ACTIVIDAD_MODALIDAD (
MODALIDAD_ID
);

/*==============================================================*/
/* Table: ACTIVIDAD_OFICINA                                     */
/*==============================================================*/
create table ACTIVIDAD_OFICINA (
   ACTIVIDAD_ID         INT4                 not null,
   OFICINA_ID           INT4                 not null,
   constraint PK_ACTIVIDAD_OFICINA primary key (ACTIVIDAD_ID, OFICINA_ID)
);

comment on table ACTIVIDAD_OFICINA is
'Indica en que oficina se puede realizar cierta actividad.';

comment on column ACTIVIDAD_OFICINA.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column ACTIVIDAD_OFICINA.OFICINA_ID is
'Identificador primario de una oficina.';

/*==============================================================*/
/* Index: ACTIVIDAD_OFICINA_PK                                  */
/*==============================================================*/
create unique index ACTIVIDAD_OFICINA_PK on ACTIVIDAD_OFICINA (
ACTIVIDAD_ID,
OFICINA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_52_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_52_FK on ACTIVIDAD_OFICINA (
ACTIVIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_53_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_53_FK on ACTIVIDAD_OFICINA (
OFICINA_ID
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA (
   CATEGORIA_ID         SERIAL               not null,
   CATEGORIA_TITULO     VARCHAR(30)          null,
   CATEGORIA_DESCRIPCION VARCHAR(200)         null,
   constraint PK_CATEGORIA primary key (CATEGORIA_ID)
);

comment on table CATEGORIA is
'Representa a un conjunto que une a trámites que poseen un aspecto en común.';

comment on column CATEGORIA.CATEGORIA_ID is
'identificador primario de una categoría.';

comment on column CATEGORIA.CATEGORIA_TITULO is
'Título con el que se maneja una categoría.';

comment on column CATEGORIA.CATEGORIA_DESCRIPCION is
'Breve descripción del aspecto en común que representa la categoría.';

/*==============================================================*/
/* Index: CATEGORIA_PK                                          */
/*==============================================================*/
create unique index CATEGORIA_PK on CATEGORIA (
CATEGORIA_ID
);

/*==============================================================*/
/* Table: CATEGORIAS_TRAMITE                                    */
/*==============================================================*/
create table CATEGORIAS_TRAMITE (
   TRAMITE_ID           INT4                 not null,
   CATEGORIA_ID         INT4                 not null,
   constraint PK_CATEGORIAS_TRAMITE primary key (TRAMITE_ID, CATEGORIA_ID)
);

comment on table CATEGORIAS_TRAMITE is
'Indica a que categoría pertenece un trámite, o bien, que trámites tiene una categoría.';

comment on column CATEGORIAS_TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column CATEGORIAS_TRAMITE.CATEGORIA_ID is
'identificador primario de una categoría.';

/*==============================================================*/
/* Index: CATEGORIAS_TRAMITE_PK                                 */
/*==============================================================*/
create unique index CATEGORIAS_TRAMITE_PK on CATEGORIAS_TRAMITE (
TRAMITE_ID,
CATEGORIA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on CATEGORIAS_TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on CATEGORIAS_TRAMITE (
CATEGORIA_ID
);

/*==============================================================*/
/* Table: COMUNA                                                */
/*==============================================================*/
create table COMUNA (
   COMUNA_ID            SERIAL               not null,
   PROVINCIA_ID         INT4                 not null,
   COMUNA_NOMBRE        VARCHAR(25)          null,
   constraint PK_COMUNA primary key (COMUNA_ID)
);

comment on table COMUNA is
'Representa una comuna perteneciente a una provincia de una región del pais.';

comment on column COMUNA.COMUNA_ID is
'Identificador primario de una comuna.';

comment on column COMUNA.PROVINCIA_ID is
'Identificador primario de una provincia.';

comment on column COMUNA.COMUNA_NOMBRE is
'Nombre que posee la comuna.';

/*==============================================================*/
/* Index: COMUNA_PK                                             */
/*==============================================================*/
create unique index COMUNA_PK on COMUNA (
COMUNA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_32_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_32_FK on COMUNA (
PROVINCIA_ID
);

/*==============================================================*/
/* Table: DOCUMENTO                                             */
/*==============================================================*/
create table DOCUMENTO (
   DOCUMENTO_ID         SERIAL               not null,
   DOCUMENTO_TITULO     VARCHAR(100)         null,
   DOCUMENTO_TIPO       VARCHAR(25)          null,
   DOCUMENTO_DESCRIPCION VARCHAR(500)         null,
   constraint PK_DOCUMENTO primary key (DOCUMENTO_ID)
);

comment on table DOCUMENTO is
'Representa algun documento que se solicita en una actividad para la realización de un trámite.';

comment on column DOCUMENTO.DOCUMENTO_ID is
'Identificador primario de un documento específico';

comment on column DOCUMENTO.DOCUMENTO_TITULO is
'Título con que se maneja el documento.';

comment on column DOCUMENTO.DOCUMENTO_TIPO is
'Indica el tipo de documento que es, ya sea: Certificado, Formulario, Fotocopia, Contrato, entre otros.';

comment on column DOCUMENTO.DOCUMENTO_DESCRIPCION is
'Breve descripción de a que corresponde este documento.';

/*==============================================================*/
/* Index: DOCUMENTO_PK                                          */
/*==============================================================*/
create unique index DOCUMENTO_PK on DOCUMENTO (
DOCUMENTO_ID
);

/*==============================================================*/
/* Table: DOCUMENTO_TRAMITE                                     */
/*==============================================================*/
create table DOCUMENTO_TRAMITE (
   TRAMITE_ID           INT4                 not null,
   DOCUMENTO_ID         INT4                 not null,
   constraint PK_DOCUMENTO_TRAMITE primary key (TRAMITE_ID, DOCUMENTO_ID)
);

comment on table DOCUMENTO_TRAMITE is
'Indica los documentos necesarios que se necesitan durante el proceso completo de realización de un trámite.';

comment on column DOCUMENTO_TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column DOCUMENTO_TRAMITE.DOCUMENTO_ID is
'Identificador primario de un documento específico';

/*==============================================================*/
/* Index: DOCUMENTO_TRAMITE_PK                                  */
/*==============================================================*/
create unique index DOCUMENTO_TRAMITE_PK on DOCUMENTO_TRAMITE (
TRAMITE_ID,
DOCUMENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_40_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_40_FK on DOCUMENTO_TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_41_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_41_FK on DOCUMENTO_TRAMITE (
DOCUMENTO_ID
);

/*==============================================================*/
/* Table: EDICION                                               */
/*==============================================================*/
create table EDICION (
   EDICION_ID           SERIAL                 not null,
   USUARIO_ID           INT4                 null,
   TRAMITE_ID           INT4                 null,
   EDICION_FECHA        DATE                 null,
   EDICION_TIPO         TIPO_EDICION         null,
   EDICION_DESCRIPCION  VARCHAR(200)         null,
   constraint PK_EDICION primary key (EDICION_ID)
);

comment on table EDICION is
'Corresponde a un historial que almacena los cambios que realiza un administrador a cierto trámite, de manera de tener un manejo y un conocimiento de las actualizaciones y los responsables de estas.';

comment on column EDICION.EDICION_ID is
'Identificador primario de una edición';

comment on column EDICION.USUARIO_ID is
'Identificador primario de un usuario.';

comment on column EDICION.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column EDICION.EDICION_FECHA is
'Fecha en la cual se realizo la actualización de algun campo de un trámite.';

comment on column EDICION.EDICION_TIPO is
'Indica que tipo de edición se llevo a cabo en un trámite, puede ser: Creación(C), Eliminación(E) y Bloqueo(B).';

comment on column EDICION.EDICION_DESCRIPCION is
'Descripción detallada de los cambios realizados, especificando que es lo que hizo, sobre que lo hizo y por que lo hizo.';

/*==============================================================*/
/* Index: EDICION_PK                                            */
/*==============================================================*/
create unique index EDICION_PK on EDICION (
EDICION_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_45_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_45_FK on EDICION (
USUARIO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_46_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_46_FK on EDICION (
TRAMITE_ID
);

/*==============================================================*/
/* Table: ETAPA                                                 */
/*==============================================================*/
create table ETAPA (
   ACTIVIDAD_ID         INT4                 not null,
   MODALIDAD_ACT_ID     INT4                 not null,
   ACTIVIDAD_SIG_ID     INT4                 not null,
   MODALIDAD_ACT_SIG_ID INT4                 not null,
   TRAMITE_ID           INT4                 not null,
   MODALIDAD_TRA_ID     INT4                 not null,
   ETAPA_NRO            INT4                 null,
   ETAPA_ESPERA         INT4                 null,
   constraint PK_ETAPA primary key (ACTIVIDAD_ID, MODALIDAD_ACT_ID, ACTIVIDAD_SIG_ID, MODALIDAD_ACT_SIG_ID, TRAMITE_ID, MODALIDAD_TRA_ID)
);

comment on table ETAPA is
'Representa a cada étapa a pasar para la realización de un trámite dada la modalidad y situacion que el usuario necesite. Esta permite entregar una secuencialidad de las actividades para informar del trámite en orden de realización. Una etapa muestra una actividad y la siguiente a esta.';

comment on column ETAPA.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column ETAPA.MODALIDAD_ACT_ID is
'Identificador primario de una modalidad.';

comment on column ETAPA.ACTIVIDAD_SIG_ID is
'Identificador primario de una actividad.';

comment on column ETAPA.MODALIDAD_ACT_SIG_ID is
'Identificador primario de una modalidad.';

comment on column ETAPA.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column ETAPA.MODALIDAD_TRA_ID is
'Identificador primario de una modalidad.';

comment on column ETAPA.ETAPA_NRO is
'Número que se manejara para una etapa de un trámite.';

/*==============================================================*/
/* Index: ETAPA_PK                                              */
/*==============================================================*/
create unique index ETAPA_PK on ETAPA (
ACTIVIDAD_ID,
MODALIDAD_ACT_ID,
ACTIVIDAD_SIG_ID,
MODALIDAD_ACT_SIG_ID,
TRAMITE_ID,
MODALIDAD_TRA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_16_FK on ETAPA (
ACTIVIDAD_ID,
MODALIDAD_ACT_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_17_FK on ETAPA (
ACTIVIDAD_SIG_ID,
MODALIDAD_ACT_SIG_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_37_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_37_FK on ETAPA (
TRAMITE_ID,
MODALIDAD_TRA_ID
);

/*==============================================================*/
/* Table: FICHA_TECNICA                                         */
/*==============================================================*/
create table FICHA_TECNICA (
   TRAMITE_ID           INT4                 not null,
   FICHA_NUMERO_VISITAS INT4                 null,
   FICHA_FECHA_CREACION DATE                 null,
   FICHA_FECHA_ACTUALIZADO DATE                 null,
   FICHA_ESTADO         ESTADO_TRAMITE       null,
   FICHA_COMENTARIO_ESTADO VARCHAR(200)         null,
   FICHA_DESTACADO      BOOL                 null,
   constraint PK_FICHA_TECNICA primary key (TRAMITE_ID)
);

comment on table FICHA_TECNICA is
'Representa la ficha de un trámite que contiene información de utilidad para otras funcionalidades del sistema, permitiendo   saber la importancia de un trámite entre los usuarios o dado el contexto.';

comment on column FICHA_TECNICA.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column FICHA_TECNICA.FICHA_NUMERO_VISITAS is
'Indica la cantidad de vistas que ha tenido un trámite por parte de los usuarios.';

comment on column FICHA_TECNICA.FICHA_FECHA_CREACION is
'Indica la fecha en la cual el trámite fue ingresado al sistema.';

comment on column FICHA_TECNICA.FICHA_FECHA_ACTUALIZADO is
'Indica la última fecha en que un trámite fue actualizado.';

comment on column FICHA_TECNICA.FICHA_ESTADO is
'Indica el estado en que se encuentra un trámite, este puede ser: "Ingresado pero no activo"(NA), "Activo"(A), "En revisión"(R), "Bloqueado"(B) y "Eliminado"(E).';

comment on column FICHA_TECNICA.FICHA_COMENTARIO_ESTADO is
'Descripción que agrega mas información con respecto al estado del trámite, como puede ser el indicar las razones de un estado específico.';

comment on column FICHA_TECNICA.FICHA_DESTACADO is
'Indica si un trámite es destacado por sobre los demás para aparecer dentro de la lista que se muestra en la página principal. Este es un booleano indicando SI (1) o NO (0).';

/*==============================================================*/
/* Index: FICHA_TECNICA_PK                                      */
/*==============================================================*/
create unique index FICHA_TECNICA_PK on FICHA_TECNICA (
TRAMITE_ID
);

/*==============================================================*/
/* Table: FORMATO_DOCUMENTO                                     */
/*==============================================================*/
create table FORMATO_DOCUMENTO (
   FORMATO_DOC_ID       SERIAL               not null,
   DOCUMENTO_ID         INT4                 not null,
   FORMATO_DOC_NOMBRE   FORMATO_DOC          null,
   FORMATO_DOC_COSTO    INT4                 null,
   FORMATO_DOC_ARCHIVO  CHAR(254)            null,
   constraint PK_FORMATO_DOCUMENTO primary key (FORMATO_DOC_ID)
);

comment on table FORMATO_DOCUMENTO is
'Corresponde al formato en que se encuentra disponible un documento.';

comment on column FORMATO_DOCUMENTO.FORMATO_DOC_ID is
'Indentificado primeario del formato de un documento.';

comment on column FORMATO_DOCUMENTO.DOCUMENTO_ID is
'Identificador primario de un documento específico';

comment on column FORMATO_DOCUMENTO.FORMATO_DOC_NOMBRE is
'Nombre del formato en que se encuentra disponible el documento. Este puede ser "Físico"(F) o "Virtual"(V).';

comment on column FORMATO_DOCUMENTO.FORMATO_DOC_COSTO is
'Indica el costo que tiene obtener el documento en tal formato.';

comment on column FORMATO_DOCUMENTO.FORMATO_DOC_ARCHIVO is
'Archivo que muestra un ejemplo o representación del documento bajo cierto formato.';

/*==============================================================*/
/* Index: FORMATO_DOCUMENTO_PK                                  */
/*==============================================================*/
create unique index FORMATO_DOCUMENTO_PK on FORMATO_DOCUMENTO (
FORMATO_DOC_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_49_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_49_FK on FORMATO_DOCUMENTO (
DOCUMENTO_ID
);

/*==============================================================*/
/* Table: LEY                                                   */
/*==============================================================*/
create table LEY (
   LEY_ID               SERIAL               not null,
   LEY_TITULO           VARCHAR(50)          null,
   LEY_DESCRIPCION      VARCHAR(200)         null,
   constraint PK_LEY primary key (LEY_ID)
);

comment on table LEY is
'Representa a una norma, ley, decreto, entre otros, del estado o el mundo privado, la cual esta tras la realización de un trámite.';

comment on column LEY.LEY_ID is
'Identificador primario de la ley.';

comment on column LEY.LEY_TITULO is
'Número de la ley del estado.';

comment on column LEY.LEY_DESCRIPCION is
'Breve descripción de que se trata la ley y de lo que corresponda al trámite.';

/*==============================================================*/
/* Index: LEY_PK                                                */
/*==============================================================*/
create unique index LEY_PK on LEY (
LEY_ID
);

/*==============================================================*/
/* Table: LOG_SQL                                               */
/*==============================================================*/
create table LOG_SQL (
   LOG_ID               SERIAL               not null,
   LOG_FECHA            DATE                 null,
   LOG_TABLA            VARCHAR(30)          null,
   LOG_USUARIO          VARCHAR(100)         null,
   LOG_OPERACION        OPERACION_LOG        null,
   LOG_ANTES            VARCHAR(2000)        null,
   LOG_DESPUES          VARCHAR(2000)        null,
   constraint PK_LOG_SQL primary key (LOG_ID)
);

comment on table LOG_SQL is
'Corresponde al registro de las acciones (SELECT, INSERT, DELETE o UPDATE)realizados sobre ciertas tablas en la base de datos, indicando que se hizo, quien lo hizo y cuando lo hizo.';

comment on column LOG_SQL.LOG_ID is
'Indentificador primario de un LOG_SQL';

comment on column LOG_SQL.LOG_FECHA is
'Fecha en la que se realizo una acción en la BD.';

comment on column LOG_SQL.LOG_TABLA is
'Tabla sobre la que se hizo la acción.';

comment on column LOG_SQL.LOG_USUARIO is
'Indica que usuario hizo la accion';

comment on column LOG_SQL.LOG_OPERACION is
'Acción que se hizo sobre la BD';

comment on column LOG_SQL.LOG_ANTES is
'Tupla antes de la accion realizada, si es que la hay.';

comment on column LOG_SQL.LOG_DESPUES is
'Tupla despues de la accion realizada, si es que la hay';

/*==============================================================*/
/* Index: LOG_SQL_PK                                            */
/*==============================================================*/
create unique index LOG_SQL_PK on LOG_SQL (
LOG_ID
);

/*==============================================================*/
/* Table: MARCADO                                               */
/*==============================================================*/
create table MARCADO (
   TRAMITE_ID           INT4                 not null,
   USUARIO_ID           INT4                 not null,
   MARCADO_FECHA        DATE                 null,
   MARCADO_NOTIFICACION BOOL                 null,
   constraint PK_MARCADO primary key (TRAMITE_ID, USUARIO_ID)
);

comment on table MARCADO is
'Indica la lista de "Mis trámites marcados" de un usuario la cual corresponde a una lista de favoritos, lo cual permite estar al tanto de los cambios de estos y de tenerlos siempre con un acceso mas directo.';

comment on column MARCADO.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column MARCADO.USUARIO_ID is
'Identificador primario de un usuario.';

comment on column MARCADO.MARCADO_FECHA is
'Indica la fecha en que un usuario marco un trámite.';

comment on column MARCADO.MARCADO_NOTIFICACION is
'Indica si el usuario desea recibir notificaciones del trámite marcado (1) o no (0)';

/*==============================================================*/
/* Index: MARCADO_PK                                            */
/*==============================================================*/
create unique index MARCADO_PK on MARCADO (
TRAMITE_ID,
USUARIO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_26_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_26_FK on MARCADO (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_27_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_27_FK on MARCADO (
USUARIO_ID
);

/*==============================================================*/
/* Table: MARCO_LEGAL_TRAMITE                                   */
/*==============================================================*/
create table MARCO_LEGAL_TRAMITE (
   TRAMITE_ID           INT4                 not null,
   LEY_ID               INT4                 not null,
   constraint PK_MARCO_LEGAL_TRAMITE primary key (TRAMITE_ID, LEY_ID)
);

comment on table MARCO_LEGAL_TRAMITE is
'Indica el marco legal, es decir, las leyes, decretos, normas u otros, que estan tras la realización de un trámite.';

comment on column MARCO_LEGAL_TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column MARCO_LEGAL_TRAMITE.LEY_ID is
'Identificador primario de la ley.';

/*==============================================================*/
/* Index: MARCO_LEGAL_TRAMITE_PK                                */
/*==============================================================*/
create unique index MARCO_LEGAL_TRAMITE_PK on MARCO_LEGAL_TRAMITE (
TRAMITE_ID,
LEY_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on MARCO_LEGAL_TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on MARCO_LEGAL_TRAMITE (
LEY_ID
);

/*==============================================================*/
/* Table: MODALIDAD                                             */
/*==============================================================*/
create table MODALIDAD (
   MODALIDAD_ID         SERIAL               not null,
   MODALIDAD_TITULO     VARCHAR(50)          null,
   MODALIDAD_DESCRIPCION VARCHAR(100)         null,
   constraint PK_MODALIDAD primary key (MODALIDAD_ID)
);

comment on table MODALIDAD is
'Representa a las diferentes maneras en las que se puede desarrollar un mismo trámite, como por ejemplo: "En ofinas", indicando que se puede hacer presencialmente, "En línea", si se puede hacer por internet, "Por telefóno", entre otros.';

comment on column MODALIDAD.MODALIDAD_ID is
'Identificador primario de una modalidad.';

comment on column MODALIDAD.MODALIDAD_TITULO is
'Título que se ultilliza para indicar la modalidad.';

comment on column MODALIDAD.MODALIDAD_DESCRIPCION is
'Breve descripción de que consiste la  modalidad.';

/*==============================================================*/
/* Index: MODALIDAD_PK                                          */
/*==============================================================*/
create unique index MODALIDAD_PK on MODALIDAD (
MODALIDAD_ID
);

/*==============================================================*/
/* Table: NOTIFICACION                                          */
/*==============================================================*/
create table NOTIFICACION (
   NOTIFICACION_ID      SERIAL               not null,
   NOTIFICACION_TITULO  VARCHAR(150)          null,
   NOTIFICACION_DESCRIPCION VARCHAR(250)         null,
   constraint PK_NOTIFICACION primary key (NOTIFICACION_ID)
);

comment on table NOTIFICACION is
'Mensaje corto que indica a un usuario la ocurrencia de un hecho de importancia para él. Este va a indicar si un trámite marcado ha tenido un cambio importante, si ha llegado una solicitud de información o una orden de trabajo.';

comment on column NOTIFICACION.NOTIFICACION_ID is
'Identificador primario de una notificación.';

comment on column NOTIFICACION.NOTIFICACION_TITULO is
'Nombre corto que representa una notificación.';

comment on column NOTIFICACION.NOTIFICACION_DESCRIPCION is
'Mensaje completo que indica a que se debe la notificación.';

/*==============================================================*/
/* Index: NOTIFICACION_PK                                       */
/*==============================================================*/
create unique index NOTIFICACION_PK on NOTIFICACION (
NOTIFICACION_ID
);

/*==============================================================*/
/* Table: OFICINA                                               */
/*==============================================================*/
create table OFICINA (
   OFICINA_ID           SERIAL               not null,
   COMUNA_ID            INT4                 not null,
   ORGANISMO_ID         INT4                 not null,
   OFICINA_NOMBRE       VARCHAR(100)         null,
   OFICINA_DIRECCION    VARCHAR(150)         null,
   OFICINA_TEL_FIJO     VARCHAR(25)          null,
   OFICINA_TEL_CELU     VARCHAR(25)          null,
   OFICINA_HORARIO      VARCHAR(150)         null,
   constraint PK_OFICINA primary key (OFICINA_ID)
);

comment on table OFICINA is
'Representa el lugar físico o edificio perteneciente a un organismo donde se debe realizar alguna actividad de un trámite.';

comment on column OFICINA.OFICINA_ID is
'Identificador primario de una oficina.';

comment on column OFICINA.COMUNA_ID is
'Identificador primario de una comuna.';

comment on column OFICINA.ORGANISMO_ID is
'Identificador primario de un organismo.';

comment on column OFICINA.OFICINA_NOMBRE is
'Nombre que posee una oficina.';

comment on column OFICINA.OFICINA_DIRECCION is
'Texto que posee la dirección de una oficina compuesto por el nombre de la calle, número y número de departamento en caso de ser necesario.';

comment on column OFICINA.OFICINA_TEL_FIJO is
'Número de teléfono con el cual contactarse con el módulo de atención de una oficina desde una linea fija.';

comment on column OFICINA.OFICINA_TEL_CELU is
'Número de teléfono para comunicarse con el módulo de atención de una oficina desde un celular.';

comment on column OFICINA.OFICINA_HORARIO is
'Texto que indica el horario de atención que posee una oficina indicando los dias de atención y los rangos de hora que atiende.';

/*==============================================================*/
/* Index: OFICINA_PK                                            */
/*==============================================================*/
create unique index OFICINA_PK on OFICINA (
OFICINA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_25_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_25_FK on OFICINA (
ORGANISMO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_39_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_39_FK on OFICINA (
COMUNA_ID
);

/*==============================================================*/
/* Table: ORDEN_TRABAJO                                         */
/*==============================================================*/
create table ORDEN_TRABAJO (
   ORDEN_ID             SERIAL               not null,
   USUARIO_ENVIA_ID     INT4                 null,
   USUARIO_RECIBE_ID    INT4                 null,
   ORDEN_ASUNTO         VARCHAR(100)         null,
   ORDEN_DESCRIPCION    VARCHAR(1000)        null,
   ORDEN_FECHA          DATE                 null,
   ORDEN_FECHA_ENTREGA  DATE                 null,
   ORDEN_ESTADO         ESTADO_ORDEN         null,
   constraint PK_ORDEN_TRABAJO primary key (ORDEN_ID)
);

comment on table ORDEN_TRABAJO is
'Representa a la orden que un adminsitrador del sistema envia a un adminsitrador de información, indicandole que debe realizar algun trabajo sobre el sistema, ya sea crear, editar o borrar alguna información.';

comment on column ORDEN_TRABAJO.ORDEN_ID is
'Identificador primario de una orden de trabajo.';

comment on column ORDEN_TRABAJO.USUARIO_ENVIA_ID is
'Identificador primario de un usuario.';

comment on column ORDEN_TRABAJO.USUARIO_RECIBE_ID is
'Identificador primario de un usuario.';

comment on column ORDEN_TRABAJO.ORDEN_ASUNTO is
'Indica el asunto por el que se envia la orden.';

comment on column ORDEN_TRABAJO.ORDEN_DESCRIPCION is
'Detalle de que consiste la orden enviada.';

comment on column ORDEN_TRABAJO.ORDEN_FECHA is
'Fecha de envio de la orden de trabajo.';

comment on column ORDEN_TRABAJO.ORDEN_FECHA_ENTREGA is
'Fecha en la que la orden debe ser cumplida.';

comment on column ORDEN_TRABAJO.ORDEN_ESTADO is
'Estado en el que se encuentra la orden, puede ser: Enviada(E), Leida(L), Realizada(R), Archivada(A).';

/*==============================================================*/
/* Index: ORDEN_TRABAJO_PK                                      */
/*==============================================================*/
create unique index ORDEN_TRABAJO_PK on ORDEN_TRABAJO (
ORDEN_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_44_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_44_FK on ORDEN_TRABAJO (
USUARIO_RECIBE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_47_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_47_FK on ORDEN_TRABAJO (
USUARIO_ENVIA_ID
);

/*==============================================================*/
/* Table: ORGANISMO                                             */
/*==============================================================*/
create table ORGANISMO (
   ORGANISMO_ID         SERIAL               not null,
   ORGANISMO_NOMBRE     VARCHAR(100)         null,
   ORGANISMO_DESCRIPCION VARCHAR(1000)        null,
   ORGANISMO_TEL_FIJO   VARCHAR(25)          null,
   ORGANISMO_TEL_CELU   VARCHAR(25)          null,
   ORGANISMO_PAGINA_WEB VARCHAR(50)          null,
   ORGANISMO_LOGO       CHAR(254)            null,
   constraint PK_ORGANISMO primary key (ORGANISMO_ID)
);

comment on table ORGANISMO is
'Representa al organismo en el cual se debe desarrollar un trámite y quien impone como se debe realizar.';

comment on column ORGANISMO.ORGANISMO_ID is
'Identificador primario de un organismo.';

comment on column ORGANISMO.ORGANISMO_NOMBRE is
'Nombre completo que posee el organismo.';

comment on column ORGANISMO.ORGANISMO_DESCRIPCION is
'Una breve descripción del organismo indicando aspectos como de que tipo es y de que se encarga o realiza.';

comment on column ORGANISMO.ORGANISMO_TEL_FIJO is
'Número de teléfono que permite comunicar con el módulo de atención central de un organismo desde una linea fija.';

comment on column ORGANISMO.ORGANISMO_TEL_CELU is
'Número de telefono que permite comunicar con el módulo de atención central de un organismo desde un celular.';

comment on column ORGANISMO.ORGANISMO_PAGINA_WEB is
'Dirección web de la página del organismo.';

/*==============================================================*/
/* Index: ORGANISMO_PK                                          */
/*==============================================================*/
create unique index ORGANISMO_PK on ORGANISMO (
ORGANISMO_ID
);

/*==============================================================*/
/* Table: PALABRA_CLAVE                                         */
/*==============================================================*/
create table PALABRA_CLAVE (
   PALABRA_ID           SERIAL               not null,
   PALABRA_NOMBRE       VARCHAR(50)          null,
   constraint PK_PALABRA_CLAVE primary key (PALABRA_ID)
);

comment on table PALABRA_CLAVE is
'Representa a una palabra clave o etiqueta que describe algún objeto o dato.';

comment on column PALABRA_CLAVE.PALABRA_ID is
'Identificador primario de una palabra clave.';

comment on column PALABRA_CLAVE.PALABRA_NOMBRE is
'Corresponde a la palabra clave.';

/*==============================================================*/
/* Index: PALABRA_CLAVE_PK                                      */
/*==============================================================*/
create unique index PALABRA_CLAVE_PK on PALABRA_CLAVE (
PALABRA_ID
);

/*==============================================================*/
/* Table: PALABRA_FICHA_TRAMITE                                 */
/*==============================================================*/
create table PALABRA_FICHA_TRAMITE (
   TRAMITE_ID           INT4                 not null,
   PALABRA_ID           INT4                 not null,
   constraint PK_PALABRA_FICHA_TRAMITE primary key (TRAMITE_ID, PALABRA_ID)
);

comment on table PALABRA_FICHA_TRAMITE is
'Indica las palabras claves que posee un trámite, lo cual ayuda a la busqueda de estos desde el buscador implementado en el sistema.';

comment on column PALABRA_FICHA_TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column PALABRA_FICHA_TRAMITE.PALABRA_ID is
'Identificador primario de una palabra clave.';

/*==============================================================*/
/* Index: PALABRA_FICHA_TRAMITE_PK                              */
/*==============================================================*/
create unique index PALABRA_FICHA_TRAMITE_PK on PALABRA_FICHA_TRAMITE (
TRAMITE_ID,
PALABRA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_35_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_35_FK on PALABRA_FICHA_TRAMITE (
PALABRA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_36_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_36_FK on PALABRA_FICHA_TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Table: PROVINCIA                                             */
/*==============================================================*/
create table PROVINCIA (
   PROVINCIA_ID         SERIAL               not null,
   REGION_ID            INT4                 not null,
   PROVINCIA_NOMBRE     VARCHAR(30)          null,
   constraint PK_PROVINCIA primary key (PROVINCIA_ID)
);

comment on table PROVINCIA is
'Representa la provincia de una región del pais.';

comment on column PROVINCIA.PROVINCIA_ID is
'Identificador primario de una provincia.';

comment on column PROVINCIA.REGION_ID is
'Identificador primario de una región.';

comment on column PROVINCIA.PROVINCIA_NOMBRE is
'Nombre que posee una provincia.';

/*==============================================================*/
/* Index: PROVINCIA_PK                                          */
/*==============================================================*/
create unique index PROVINCIA_PK on PROVINCIA (
PROVINCIA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_31_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_31_FK on PROVINCIA (
REGION_ID
);

/*==============================================================*/
/* Table: REGION                                                */
/*==============================================================*/
create table REGION (
   REGION_ID            SERIAL               not null,
   REGION_NOMBRE        VARCHAR(60)          null,
   constraint PK_REGION primary key (REGION_ID)
);

comment on table REGION is
'Representa a una región del pais.';

comment on column REGION.REGION_ID is
'Identificador primario de una región.';

comment on column REGION.REGION_NOMBRE is
'Nombre de la región incluyendose el número de esta.';

/*==============================================================*/
/* Index: REGION_PK                                             */
/*==============================================================*/
create unique index REGION_PK on REGION (
REGION_ID
);

/*==============================================================*/
/* Table: RELACIONADOS                                          */
/*==============================================================*/
create table RELACIONADOS (
   TRAMITE_ID           INT4                 not null,
   TRAMITE_REL_ID       INT4                 not null,
   RELACIONADOS_INFO    VARCHAR(300)         null,
   constraint PK_RELACIONADOS primary key (TRAMITE_ID, TRAMITE_REL_ID)
);

comment on table RELACIONADOS is
'Conjunto de trámites que tiene alguna relación con otro trámite, para ser mostrada a un lado de la información de un trámite.';

comment on column RELACIONADOS.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column RELACIONADOS.TRAMITE_REL_ID is
'Identificador primario de un trámite.';

comment on column RELACIONADOS.RELACIONADOS_INFO is
'Información relevante con respecto a la relación entre trámites';

/*==============================================================*/
/* Index: RELACIONADOS_PK                                       */
/*==============================================================*/
create unique index RELACIONADOS_PK on RELACIONADOS (
TRAMITE_ID,
TRAMITE_REL_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_55_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_55_FK on RELACIONADOS (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_56_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_56_FK on RELACIONADOS (
TRAMITE_REL_ID
);

/*==============================================================*/
/* Table: REQUISITO                                             */
/*==============================================================*/
create table REQUISITO (
   REQUISITO_ID         SERIAL               not null,
   REQUISITO_TITULO     VARCHAR(50)          null,
   REQUISITO_DESCRIPCION VARCHAR(200)         null,
   constraint PK_REQUISITO primary key (REQUISITO_ID)
);

comment on table REQUISITO is
'Representa a una condición que debe cumplir un usuario para poder realizar un trámite.';

comment on column REQUISITO.REQUISITO_ID is
'Identificador primario de un requisito.';

comment on column REQUISITO.REQUISITO_TITULO is
'Título con el que se maneja un requisito.';

comment on column REQUISITO.REQUISITO_DESCRIPCION is
'Detalle de que se trata un requisito específico.';

/*==============================================================*/
/* Index: REQUISITO_PK                                          */
/*==============================================================*/
create unique index REQUISITO_PK on REQUISITO (
REQUISITO_ID
);

/*==============================================================*/
/* Table: REQUISITOS_TRAMITE                                    */
/*==============================================================*/
create table REQUISITOS_TRAMITE (
   TRAMITE_ID           INT4                 not null,
   REQUISITO_ID         INT4                 not null,
   constraint PK_REQUISITOS_TRAMITE primary key (TRAMITE_ID, REQUISITO_ID)
);

comment on table REQUISITOS_TRAMITE is
'Indica los requisitos que tiene un trámite para poder ser realizado.';

comment on column REQUISITOS_TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column REQUISITOS_TRAMITE.REQUISITO_ID is
'Identificador primario de un requisito.';

/*==============================================================*/
/* Index: REQUISITOS_TRAMITE_PK                                 */
/*==============================================================*/
create unique index REQUISITOS_TRAMITE_PK on REQUISITOS_TRAMITE (
TRAMITE_ID,
REQUISITO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on REQUISITOS_TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on REQUISITOS_TRAMITE (
REQUISITO_ID
);

/*==============================================================*/
/* Table: SOLICITUD                                             */
/*==============================================================*/
create table SOLICITUD (
   SOLICITUD_ID         SERIAL               not null,
   USUARIO_ENVIA_ID     INT4                 null,
   USUARIO_RECIBE_ID    INT4                 null,
   SOLICITUD_TIPO       TIPO_SOLICITUD       null,
   SOLICITUD_MENSAJE    VARCHAR(500)         null,
   SOLICITUD_FECHA      DATE                 null,
   SOLICITUD_ARCHIVADA  BOOL                 null,
   constraint PK_SOLICITUD primary key (SOLICITUD_ID)
);

comment on table SOLICITUD is
'Corresponde a un mensaje enviado por un usuario consultante al administrador del sistema, solcitando información con respecto a un trámote, organismo o documento.
';

comment on column SOLICITUD.SOLICITUD_ID is
'identificador primario de una solicitud.';

comment on column SOLICITUD.USUARIO_ENVIA_ID is
'Identificador primario de un usuario.';

comment on column SOLICITUD.USUARIO_RECIBE_ID is
'Identificador primario de un usuario.';

comment on column SOLICITUD.SOLICITUD_TIPO is
'Señala si la solicitud es con respecto a un trámite (T), organismo (O) o documento(D)';

comment on column SOLICITUD.SOLICITUD_MENSAJE is
'Corresponde al cuerpo del mensaje donde el usuario especifica que es lo que esta solicitando.';

comment on column SOLICITUD.SOLICITUD_FECHA is
'Fecha en que la solicitud fue realizada por el usuario.';

comment on column SOLICITUD.SOLICITUD_ARCHIVADA is
'Indica si la solicitud ya fue vista por el administrador y este la archivó para no aparecer en la página principal de los mensajes.';

/*==============================================================*/
/* Index: SOLICITUD_PK                                          */
/*==============================================================*/
create unique index SOLICITUD_PK on SOLICITUD (
SOLICITUD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_42_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_42_FK on SOLICITUD (
USUARIO_RECIBE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_43_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_43_FK on SOLICITUD (
USUARIO_ENVIA_ID
);

/*==============================================================*/
/* Table: TIEMPO_USUARIO                                        */
/*==============================================================*/
create table TIEMPO_USUARIO (
   USUARIO_ID           INT4                 not null,
   ACTIVIDAD_ID         INT4                 not null,
   MODALIDAD_ID         INT4                 not null,
   TIEMPO_USUARIO_MINUTOS INT4                 null,
   constraint PK_TIEMPO_USUARIO primary key (USUARIO_ID, ACTIVIDAD_ID, MODALIDAD_ID)
);

comment on table TIEMPO_USUARIO is
'Indica la estimación de tiempo que se debe ocupar para la realización de una actividad entregada por un usuario, idealmente, en base a su experiencia personal al llevar a cabo la actividad.';

comment on column TIEMPO_USUARIO.USUARIO_ID is
'Identificador primario de un usuario.';

comment on column TIEMPO_USUARIO.ACTIVIDAD_ID is
'Identificador primario de una actividad.';

comment on column TIEMPO_USUARIO.MODALIDAD_ID is
'Identificador primario de una modalidad.';

comment on column TIEMPO_USUARIO.TIEMPO_USUARIO_MINUTOS is
'Estimación temporal entregada por el usuario en minútos. ';

/*==============================================================*/
/* Index: TIEMPO_USUARIO_PK                                     */
/*==============================================================*/
create unique index TIEMPO_USUARIO_PK on TIEMPO_USUARIO (
USUARIO_ID,
ACTIVIDAD_ID,
MODALIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_11_FK on TIEMPO_USUARIO (
USUARIO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_20_FK on TIEMPO_USUARIO (
ACTIVIDAD_ID,
MODALIDAD_ID
);

/*==============================================================*/
/* Table: TRAMITE                                               */
/*==============================================================*/
create table TRAMITE (
   TRAMITE_ID           SERIAL               not null,
   ORGANISMO_ID         INT4                 null,
   TRAMITE_TITULO       VARCHAR(100)         null,
   TRAMITE_DESCRIPCION  VARCHAR(1000)        null,
   TRAMITE_DUR_ANUAL    BOOL                 null,
   TRAMITE_INICIO       DATE                 null,
   TRAMITE_FIN          DATE                 null,
   TRAMITE_RESULTADO    VARCHAR(300)         null,
   constraint PK_TRAMITE primary key (TRAMITE_ID)
);

comment on table TRAMITE is
'Representa a un trámite en sí, el cual almacena la información principal y se relaciona con todas las entidades que almacenan información que se necesita saber de este. Es la entidad fundamental del sistema, ya que todas las consultas se basan al trámite que se este realizando.';

comment on column TRAMITE.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column TRAMITE.ORGANISMO_ID is
'Identificador primario de un organismo.';

comment on column TRAMITE.TRAMITE_TITULO is
'Título con que se manejara un trámite.';

comment on column TRAMITE.TRAMITE_DESCRIPCION is
'Descripción general de un trámite entregando información basica para entender de que se trata lo que se esta visualizando y asi saber si es el trámite que se busca.';

comment on column TRAMITE.TRAMITE_DUR_ANUAL is
'Indica si un trámite tiene duración anual (1) o esta limitada entre fechas (0)';

comment on column TRAMITE.TRAMITE_INICIO is
'Fecha de inicio en la cual un trámite comienza a estar válido para ser llevado a cabo.';

comment on column TRAMITE.TRAMITE_FIN is
'Fecha final en la cual un trámite deja de estar válido para ser llevado a cabo.';

comment on column TRAMITE.TRAMITE_RESULTADO is
'Indica el resultado que tiene para el usuario la realización del trámite, ya sea indicando que es lo que debe realizar ahora, que es lo que puede hacer, entre otros.';

/*==============================================================*/
/* Index: TRAMITE_PK                                            */
/*==============================================================*/
create unique index TRAMITE_PK on TRAMITE (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_48_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_48_FK on TRAMITE (
ORGANISMO_ID
);

/*==============================================================*/
/* Table: TRAMITE_MODALIDAD                                     */
/*==============================================================*/
create table TRAMITE_MODALIDAD (
   TRAMITE_ID           INT4                 not null,
   MODALIDAD_ID         INT4                 not null,
   constraint PK_TRAMITE_MODALIDAD primary key (TRAMITE_ID, MODALIDAD_ID)
);

comment on table TRAMITE_MODALIDAD is
'Indica las diferentes modalidades con las que se puede desarrollar un trámite.';

comment on column TRAMITE_MODALIDAD.TRAMITE_ID is
'Identificador primario de un trámite.';

comment on column TRAMITE_MODALIDAD.MODALIDAD_ID is
'Identificador primario de una modalidad.';

/*==============================================================*/
/* Index: TRAMITE_MODALIDAD_PK                                  */
/*==============================================================*/
create unique index TRAMITE_MODALIDAD_PK on TRAMITE_MODALIDAD (
TRAMITE_ID,
MODALIDAD_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_34_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_34_FK on TRAMITE_MODALIDAD (
TRAMITE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_38_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_38_FK on TRAMITE_MODALIDAD (
MODALIDAD_ID
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   USUARIO_ID           SERIAL               not null,
   COMUNA_ID            INT4                 not null,
   USUARIO_NOMBRE       VARCHAR(50)          null,
   USUARIO_APELLIDO_PAT VARCHAR(25)          null,
   USUARIO_APELLIDO_MAT VARCHAR(25)          null,
   USUARIO_CORREO       VARCHAR(50)          null,
   USUARIO_FECHA_NACIMIENTO DATE                 null,
   USUARIO_PASSWORD     VARCHAR(25)          null,
   USUARIO_FECHA_REGISTRO DATE                 null,
   USUARIO_TIPO         TIPO_USUARIO         null,
   USUARIO_ESTADO       BOOL                 null,
   constraint PK_USUARIO primary key (USUARIO_ID)
);

comment on table USUARIO is
'Representa a toda aquella persona que usa la aplicación y se registra en el sistema. Almacena información relevante para iniciar sesíon y personalizar su cuenta.';

comment on column USUARIO.USUARIO_ID is
'Identificador primario de un usuario.';

comment on column USUARIO.COMUNA_ID is
'Identificador primario de una comuna.';

comment on column USUARIO.USUARIO_NOMBRE is
'Indica el o los nombres pilas de un usuario.';

comment on column USUARIO.USUARIO_APELLIDO_PAT is
'Indica los apellidos del usuario.';

comment on column USUARIO.USUARIO_CORREO is
'Indica el correo del usuario, el cual es muy importante para la comunicación entre usuarios, ya que es a este donde le llegarán los mensajes.';

comment on column USUARIO.USUARIO_FECHA_NACIMIENTO is
'Indica la fecha de nacimiento del usuario registrado lo cual permite conocer la edad de este.';

comment on column USUARIO.USUARIO_PASSWORD is
'Corresponde a la clave de acceso para iniciar sesión en su cuenta del sistema.';

comment on column USUARIO.USUARIO_FECHA_REGISTRO is
'Indica la fecha en la cual el usuario se registro en el sistema.';

comment on column USUARIO.USUARIO_TIPO is
'Indica si es un administrador de información (AI), el cual edita la información que se maneja con respecto a los trámites, o un administrador del sistema (AS), que a parte de realizar esas mismas acciones, administra todo lo correspondiente a las cuentas del sistema. El dominio de este atributo es AS o AI.';

comment on column USUARIO.USUARIO_ESTADO is
'Indica si un usuario puede iniciar sesión (1) o no puede hacerlo (0).';

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
USUARIO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_33_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_33_FK on USUARIO (
COMUNA_ID
);

/*==============================================================*/
/* Table: VALORACION                                            */
/*==============================================================*/
create table VALORACION (
   USUARIO_ID           INT4                 not null,
   OFICINA_ID           INT4                 not null,
   VALORACION_NUMERO    NRO_VALORACION       null,
   constraint PK_VALORACION primary key (USUARIO_ID, OFICINA_ID)
);

comment on table VALORACION is
'Indica cada valoración dada por un usuario a la atención recibida desde una oficina.';

comment on column VALORACION.USUARIO_ID is
'Identificador primario de un usuario.';

comment on column VALORACION.OFICINA_ID is
'Identificador primario de una oficina.';

comment on column VALORACION.VALORACION_NUMERO is
'Número de valoración entregada por el usuario a la atención recibida, la cual va desde 1 a 5. ';

/*==============================================================*/
/* Index: VALORACION_PK                                         */
/*==============================================================*/
create unique index VALORACION_PK on VALORACION (
USUARIO_ID,
OFICINA_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on VALORACION (
USUARIO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_28_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_28_FK on VALORACION (
OFICINA_ID
);

alter table ACTIVIDAD_DOC
   add constraint FK_ACTIVIDA_RELATIONS_ACTIVIDA foreign key (ACTIVIDAD_ID)
      references ACTIVIDAD (ACTIVIDAD_ID)
      on delete restrict on update restrict;

alter table ACTIVIDAD_DOC
   add constraint FK_ACTIVIDA_RELATIONS_DOCUMENT foreign key (DOCUMENTO_ID)
      references DOCUMENTO (DOCUMENTO_ID)
      on delete restrict on update restrict;

alter table ACTIVIDAD_MODALIDAD
   add constraint FK_ACTIVIDA_RELATIONS_ACTIVIDA foreign key (ACTIVIDAD_ID)
      references ACTIVIDAD (ACTIVIDAD_ID)
      on delete restrict on update restrict;

alter table ACTIVIDAD_MODALIDAD
   add constraint FK_ACTIVIDA_RELATIONS_MODALIDA foreign key (MODALIDAD_ID)
      references MODALIDAD (MODALIDAD_ID)
      on delete restrict on update restrict;

alter table ACTIVIDAD_OFICINA
   add constraint FK_ACTIVIDA_RELATIONS_ACTIVIDA foreign key (ACTIVIDAD_ID)
      references ACTIVIDAD (ACTIVIDAD_ID)
      on delete restrict on update restrict;

alter table ACTIVIDAD_OFICINA
   add constraint FK_ACTIVIDA_RELATIONS_OFICINA foreign key (OFICINA_ID)
      references OFICINA (OFICINA_ID)
      on delete restrict on update restrict;

alter table CATEGORIAS_TRAMITE
   add constraint FK_CATEGORI_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table CATEGORIAS_TRAMITE
   add constraint FK_CATEGORI_RELATIONS_CATEGORI foreign key (CATEGORIA_ID)
      references CATEGORIA (CATEGORIA_ID)
      on delete cascade on update restrict;

alter table COMUNA
   add constraint FK_COMUNA_RELATIONS_PROVINCI foreign key (PROVINCIA_ID)
      references PROVINCIA (PROVINCIA_ID)
      on delete restrict on update restrict;

alter table DOCUMENTO_TRAMITE
   add constraint FK_DOCUMENT_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table DOCUMENTO_TRAMITE
   add constraint FK_DOCUMENT_RELATIONS_DOCUMENT foreign key (DOCUMENTO_ID)
      references DOCUMENTO (DOCUMENTO_ID)
      on delete restrict on update restrict;

alter table EDICION
   add constraint FK_EDICION_RELATIONS_USUARIO foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID)
      on delete restrict on update restrict;

alter table EDICION
   add constraint FK_EDICION_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table ETAPA
   add constraint FK_ETAPA_RELATIONS_ACTIVIDA foreign key (ACTIVIDAD_ID, MODALIDAD_ACT_ID)
      references ACTIVIDAD_MODALIDAD (ACTIVIDAD_ID, MODALIDAD_ID)
      on delete restrict on update restrict;

alter table ETAPA
   add constraint FK_ETAPA_RELATIONS_ACTIVIDAD_S foreign key (ACTIVIDAD_SIG_ID, MODALIDAD_ACT_SIG_ID)
      references ACTIVIDAD_MODALIDAD (ACTIVIDAD_ID, MODALIDAD_ID)
      on delete restrict on update restrict;

alter table ETAPA
   add constraint FK_ETAPA_RELATIONS_TRAMITE_ foreign key (TRAMITE_ID, MODALIDAD_TRA_ID)
      references TRAMITE_MODALIDAD (TRAMITE_ID, MODALIDAD_ID)
      on delete restrict on update restrict;

alter table FICHA_TECNICA
   add constraint FK_FICHA_TE_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table FORMATO_DOCUMENTO
   add constraint FK_FORMATO__RELATIONS_DOCUMENT foreign key (DOCUMENTO_ID)
      references DOCUMENTO (DOCUMENTO_ID)
      on delete restrict on update restrict;

alter table MARCADO
   add constraint FK_MARCADO_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table MARCADO
   add constraint FK_MARCADO_RELATIONS_USUARIO foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table MARCO_LEGAL_TRAMITE
   add constraint FK_MARCO_LE_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table MARCO_LEGAL_TRAMITE
   add constraint FK_MARCO_LE_RELATIONS_LEY foreign key (LEY_ID)
      references LEY (LEY_ID)
      on delete cascade on update restrict;

alter table OFICINA
   add constraint FK_OFICINA_RELATIONS_ORGANISM foreign key (ORGANISMO_ID)
      references ORGANISMO (ORGANISMO_ID)
      on delete restrict on update restrict;

alter table OFICINA
   add constraint FK_OFICINA_RELATIONS_COMUNA foreign key (COMUNA_ID)
      references COMUNA (COMUNA_ID)
      on delete restrict on update restrict;

alter table ORDEN_TRABAJO
   add constraint FK_ORDEN_TR_RELATIONS_USUARIO_R foreign key (USUARIO_RECIBE_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table ORDEN_TRABAJO
   add constraint FK_ORDEN_TR_RELATIONS_USUARIO_E foreign key (USUARIO_ENVIA_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table PALABRA_FICHA_TRAMITE
   add constraint FK_PALABRA__RELATIONS_PALABRA_ foreign key (PALABRA_ID)
      references PALABRA_CLAVE (PALABRA_ID)
      on delete cascade on update restrict;

alter table PALABRA_FICHA_TRAMITE
   add constraint FK_PALABRA__RELATIONS_FICHA_TE foreign key (TRAMITE_ID)
      references FICHA_TECNICA (TRAMITE_ID)
      on delete cascade on update restrict;

alter table PROVINCIA
   add constraint FK_PROVINCI_RELATIONS_REGION foreign key (REGION_ID)
      references REGION (REGION_ID)
      on delete restrict on update restrict;

alter table RELACIONADOS
   add constraint FK_RELACION_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table RELACIONADOS
   add constraint FK_RELACION_RELATIONS_TRAMITE_R foreign key (TRAMITE_REL_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table REQUISITOS_TRAMITE
   add constraint FK_REQUISIT_RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table REQUISITOS_TRAMITE
   add constraint FK_REQUISIT_RELATIONS_REQUISIT foreign key (REQUISITO_ID)
      references REQUISITO (REQUISITO_ID)
      on delete cascade on update restrict;

alter table SOLICITUD
   add constraint FK_SOLICITU_RELATIONS_USUARIO_R foreign key (USUARIO_RECIBE_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table SOLICITUD
   add constraint FK_SOLICITU_RELATIONS_USUARIO_E foreign key (USUARIO_ENVIA_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table TIEMPO_USUARIO
   add constraint FK_TIEMPO_U_RELATIONS_USUARIO foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table TIEMPO_USUARIO
   add constraint FK_TIEMPO_U_RELATIONS_ACTIVIDA foreign key (ACTIVIDAD_ID, MODALIDAD_ID)
      references ACTIVIDAD_MODALIDAD (ACTIVIDAD_ID, MODALIDAD_ID)
      on delete restrict on update restrict;

alter table TRAMITE
   add constraint FK_TRAMITE_RELATIONS_ORGANISM foreign key (ORGANISMO_ID)
      references ORGANISMO (ORGANISMO_ID)
      on delete restrict on update restrict;

alter table TRAMITE_MODALIDAD
   add constraint FK_TRAMITE__RELATIONS_TRAMITE foreign key (TRAMITE_ID)
      references TRAMITE (TRAMITE_ID)
      on delete restrict on update restrict;

alter table TRAMITE_MODALIDAD
   add constraint FK_TRAMITE__RELATIONS_MODALIDA foreign key (MODALIDAD_ID)
      references MODALIDAD (MODALIDAD_ID)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_RELATIONS_COMUNA foreign key (COMUNA_ID)
      references COMUNA (COMUNA_ID)
      on delete restrict on update restrict;

alter table VALORACION
   add constraint FK_VALORACI_RELATIONS_USUARIO foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID)
      on delete cascade on update restrict;

alter table VALORACION
   add constraint FK_VALORACI_RELATIONS_OFICINA foreign key (OFICINA_ID)
      references OFICINA (OFICINA_ID)
      on delete restrict on update restrict;

