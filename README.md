#Repositorio para Proyecto Base de Datos

#Nombre del proyecto:
#Traminet

#Integrantes:

Jose Ignacio Cortés Tapia
Gonzalo Javier Martinez Ramírez
Chien-Hung Lin

#Quienes Somos:

*TRAMINET es un servicio orientado a entregar a personas 
y empresas información sobre los trámites que necesite realizar 
en su día a día, de forma detallada, simple y fiable, agilizando
aquellos momentos tediosos de la vida para liberarle horas únicas 
e irrecuperables que son necesarias en la vida laboral y doméstica

# Estructura de archivos

* En la carpeta src/ se encuentra todo lo relativo al desarrollo de django, la carpeta app_traminet contiene lo esencial para el desarrollo de las vistas, entre otros
* La carpeta static contiene todo el desarrollo web, en static/templates/ se encuentran todos los .html


# Instrucciones para la creación y poblado de la base de datos:

 ## Crear base de datos: 
	
	*Estando como superusuario postgres en el template1 ingresar: 
	
	CREATE DATABASE traminet_db OWNER traminet_owner ENCODING 'UTF8';

## Dar permisos al usuario 

# En terminal hacer: su postgres
se abre terminal de postgres: escribir:

psql traminet_db -c "GRANT ALL ON ALL TABLES IN SCHEMA public to traminet_owner;" 
psql traminet_db -c "GRANT ALL ON ALL SEQUENCES IN SCHEMA public to traminet_owner;" 
psql traminet_db -c "GRANT ALL ON ALL FUNCTIONS IN SCHEMA public to traminet_owner;" 

 ## Agregar plpython a la BD (Necesario tener instalado plpython9.3):

	*Salir del template1 e ingresar:

		createlang plpython2u traminet_db

## Scripts de la BD (Todo se encuentra en la carpeta ./DB): 

	*Haciendo uso de pgadmin agregar los scripts

	*Primero crear elementos de la BD con archivo:

		TRAMINET_DB.sql
	
	*Poblar la BD con los datos básicos escenciales para comenzar la aplicación (En carpeta ./DB/scripts_poblar) con archivo:

		poblado_basico.sql

	*Poblar la BD con un ejemplo para comenzar pruebas de triggers, vistas, sp y funcionalidades de la aplicación (En carpeta ./DB/scripts_poblar) con archivo:

		1.- poblado_ej_part1.sql
		2.- poblado_ej_part2.sql

	*Agregar todos los triggers en la carpeta ./DB/Triggers, no importa el orden

## Resultado: Todo lo esencial para la BD esta cargado
		

# Instrucciones para cargar DB en Django

* Ejecutar en src/ (donde se encuentra manage.py): python manage.py migrate
* Ejecutar en src/ (donde se encuentra manage.py): python manage.py makemigrations

## Para crear models.py que contiene la base de datos pasada a Django

* Ejecutar en src/ (donde se encuentra manage.py): python manage.py inspectdb para comprobar errores
* Ejecutar en src/ (donde se encuentra manage.py): python manage.py inspectdb > models.py genera el archivo models.py
* Borrar las primeras tres lineas de models.py (a mi me aparece la instruccion que hice y eso no deberia ir, si les aparece -> borrar)
* Copiar el models.py generado a la carpeta src/app_traminet

## En caso de error

* Cualquier modificación a la DB provoca que hay que volver a cargar la DB en Django y crear el models.py
* SIEMPRE va a tirar un error al hacer todo de nuevo, debido a que Django se confunde con las llaves foraneas
* Para esto, uno tiene que darle un nombre a estas llaves, colocando en dicha llave:  related_name= "nombre"
* Esto ocurre en las siguientes clases:

```python
class Etapa(models.Model):
    actividad = models.ForeignKey(ActividadModalidad, related_name='etapa_actividad')
    modalidad_act_id = models.IntegerField()
    actividad_sig = models.ForeignKey(ActividadModalidad, related_name='etapa_actividad_sgte')
```

```python
class OrdenTrabajo(models.Model):
    orden_id = models.IntegerField(primary_key=True)
    usuario_envia = models.ForeignKey('Usuario', related_name= 'ordentrabajo_usuario_envia')
    usuario_recibe = models.ForeignKey('Usuario', related_name= 'ordentrabajo_usuario_recibe')
    orden_asunto = models.CharField(max_length=100, blank=True)
    orden_descripcion = models.CharField(max_length=1000, blank=True)
    orden_fecha = models.DateTimeField(blank=True, null=True)
    orden_fecha_entrega = models.DateTimeField(blank=True, null=True)
    orden_estado = models.CharField(max_length=2, blank=True)
```
```python
class Solicitud(models.Model):
    solicitud_id = models.IntegerField(primary_key=True)
    usuario_envia = models.ForeignKey('Usuario', related_name='solicitud_usuario_envia')
    usuario_recibe = models.ForeignKey('Usuario', related_name='solicitud_usuario_recibe')
    solicitud_tipo = models.CharField(max_length=2, blank=True)
    solicitud_mensaje = models.CharField(max_length=500, blank=True)
    solicitud_fecha = models.DateTimeField(blank=True, null=True)
    solicitud_archivada = models.NullBooleanField()
```
```python
class Relacionados(models.Model):
    tramite = models.ForeignKey('Tramite', related_name='tramite_principal')
    tramite_rel = models.ForeignKey('Tramite', related_name='tramite_relacionado')
    relacionados_info = models.CharField(max_length=300, blank=True)
```
# Instrucciones para ejecutar la app
* Ejecutar en src/ (donde se encuentra manage.py): python manage.py runserver


# Instrucciones para el administrador
* user: admin
* pass: admin


