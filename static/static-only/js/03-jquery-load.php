<!DOCTYPE html>
<html lang="es-ES" prefix="og: http://ogp.me/ns#">
<head>
	<title>Ejemplo 1 de jQuery load(), pasando como par�metro una url</title>
	<meta name="description" content="Primer ejemplo de la func�on de jQuery load(), probamos la carga de un archivo externo pasando como par�metro la url"/>
	<meta name="keywords" content="jquery, jquery load"/>
	
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<link rel="author" href="https://plus.google.com/100521238980519601840"/>
	<link rel="publisher" href="https://plus.google.com/100521238980519601840" />
	<meta name="robots" content="noodp,noydir"/>

	<script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

	<link rel="alternate" type="application/rss+xml" title="Aner Barrena &raquo; Feed" href="http://www.anerbarrena.com/feed/" />
	<link rel='stylesheet' id='25px-css'  href='http://www.anerbarrena.com/wp-content/themes/chun/library/css/25px.min.css?ver=20110523' type='text/css' media='all' />
	<link rel='stylesheet' id='style-css'  href='http://www.anerbarrena.com/wp-content/themes/chun/style.min.css?ver=3.7.1' type='text/css' media='all' />
	<link rel='stylesheet' id='theme-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:400' type='text/css' media='all' />
	<style type="text/css" id="custom-colors-css">a, pre, code, .breadcrumb-trail a, .format-link .entry-title a .meta-nav, #respond label .required, #footer a:hover { color: #cb5700; } #branding, li.comment .comment-reply-link { background-color: #cb5700; } body { border-top-color: #cb5700; } .breaadcrumb-trail a:hover, .sticky.hentry, .loop-meta, .page-template-portfolio .hentry.page { border-bottom-color: #cb5700; } pre { border-left-color: #cb5700; } #site-title a, .entry-title, .entry-title a, .loop-title, #menu-portfolio li.current-cat a, #menu-portfolio li.current-menu-item a, .page-numbers.current { color: #050505; } .breadcrumb-trail, li.comment .comment-reply-link:hover, #footer { background-color: #050505; } .hentry, .loop-meta, .attachment-meta, #comments-template, .page-template-portfolio .hentry.page { border-top-color: #050505; } body { border-bottom-color: #050505; } #menu-primary li a { color: #00393e; } #menu-primary li li a:hover, #menu-primary li li:hover > a { background-color: #00393e; } #menu-primary li a:hover, #menu-primary li:hover > a, #menu-primary li.current-menu-item > a { color: #cc4a00; } #menu-primary li li a { background-color: #cc4a00; } #menu-primary-items ul li:first-child > a::after { border-bottom-color: #cc4a00; } #menu-primary-items ul ul li:first-child a::after { border-right-color: #cc4a00; }</style>
	<style type="text/css" id="theme-fonts-rules-css">h1, h2, h3, h4, h5, h6, th, #menu-primary li a, #menu-portfolio li a, .breadcrumb-trail, .page-links, .loop-pagination, .loop-nav, #respond input[type="submit"], #footer { font-family: 'Open Sans', sans-serif; font-weight: 400; font-style: normal; }</style>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
	<style type="text/css" id="custom-background-css">
		body.custom-background { background-image: url('http://www.anerbarrena.com/wp-content/uploads/2013/09/bkg_smoky_noise.jpg'); background-repeat: repeat; background-position: top left; background-attachment: fixed; }
	</style>
	<style type="text/css">
		h2 { text-decoration:underline; }
	</style>
		
	<!-- Responsive Select CSS 
	================================================================ -->
	<style type="text/css" id="responsive-select-css">
		.responsiveSelectContainer select.responsiveMenuSelect, select.responsiveMenuSelect{display:none;}
		@media (max-width: 960px) 
			{
			.responsiveSelectContainer{border:none !important;background:none !important;box-shadow:none !important;}
			.responsiveSelectContainer ul, ul.responsiveSelectFullMenu, #megaMenu ul.megaMenu.responsiveSelectFullMenu{display: none !important;}
			.responsiveSelectContainer select.responsiveMenuSelect, select.responsiveMenuSelect { display: inline-block; width:100%;}
			}	
	</style>
	<!-- end Responsive Select CSS -->

	<!-- Responsive Select JS
	================================================================ -->
	<script type="text/javascript">
	jQuery(document).ready( function($){
		$( '.responsiveMenuSelect' ).change(function() {
			var loc = $(this).find( 'option:selected' ).val();
			if( loc != '' && loc != '#' ) window.location = loc;
		});
		//$( '.responsiveMenuSelect' ).val('');
	});
	</script>
	<!-- end Responsive Select JS -->

		<link rel='stylesheet' id='front-estilos-css'  href='http://www.anerbarrena.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/estilos.css?ver=3.9' type='text/css' media='all' />
<script type='text/javascript' src='http://www.anerbarrena.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/_jquery.kookie.js?ver=3.9'></script>
<script type='text/javascript' src='http://www.anerbarrena.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/lib.js?ver=3.9'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cdp_cookies_info = {"url_plugin":"http:\/\/www.anerbarrena.com\/wp-content\/plugins\/asesor-cookies-para-la-ley-en-espana\/plugin.php","url_admin_ajax":"http:\/\/www.anerbarrena.com\/wp-admin\/admin-ajax.php","url_traer_aviso_php":"http:\/\/www.anerbarrena.com\/wp-content\/plugins\/asesor-cookies-para-la-ley-en-espana\/traer_aviso.php","comportamiento":"navegar"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.anerbarrena.com/wp-content/plugins/asesor-cookies-para-la-ley-en-espana/html/front/principal.js?ver=3.9'></script>	<!-- semilla GA -->
		
	
	
	
	
	
	
	
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE -->
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE-->
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE -->
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-30749552-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE -->
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE-->
	<!-- NO COPIAR ESTE CODIGO, DONT COPY THIS CODE -->

	
	
	
	
	
	
	
	
	
	
	
		
		<!-- Acci�n sobre el bot� con id=boton y actualizamos el div con id=capa -->
		<script type="text/javascript">
			$(document).ready(function() {
				$("#boton").click(function(event) {
					$("#capa").load('/demos/2013/03-jquery-load01.php');
				});
			});
		</script>
</head>
	
	<body class="wordpress ltr es_ES parent-theme y2013 m12 d22 h01 sunday logged-out custom-background home blog layout-2c-l custom-colors">
	<div id="container">
		<div class="wrap">
			<!-- #header -->
			<header id="header">
				<hgroup id="branding">
					<h1 id="site-title"><a href="http://www.anerbarrena.com" title="Aner Barrena">Aner Barrena</a></h1>
					<h2 id="site-description">Programaci&oacute;n web con PHP, JQuery, HTML5, MySQL y Apache .htaccess</h2>
				</hgroup>
			</header>

			<nav id="menu-primary" class="menu responsiveSelectContainer">
				<ul id="menu-primary-items" class="menu-items responsiveSelectFullMenu">
					<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-41">
						<a href="http://www.anerbarrena.com/programacion/php/">PHP y MySQL</a>
						<ul class="sub-menu">
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-45"><a href="http://www.anerbarrena.com/programacion/php/mysql/">MySQL</a></li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2250"><a href="http://www.anerbarrena.com/programacion/php/cadenas-string/">Funciones y operaciones con cadenas string en PHP</a></li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1093"><a href="http://www.anerbarrena.com/programacion/php/fechas/">Funciones y operaciones con fechas en PHP</a></li>
						</ul>
					</li>
					<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-42">
						<a href="http://www.anerbarrena.com/programacion/html5/">HTML5</a>
						<ul class="sub-menu">
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1125"><a href="http://www.anerbarrena.com/programacion/html5/nuevas-etiquetas-y-atributos/">Nuevos input, etiquetas y atributos en HTML5</a></li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2398"><a href="http://www.anerbarrena.com/programacion/html5/device-api-dispositivo/">HTML5 Api</a></li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1001"><a href="http://www.anerbarrena.com/programacion/html5/form-html5/">Formularios en HTML5</a></li>
						</ul>
					</li>
					<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-44">
						<a href="http://www.anerbarrena.com/programacion/jquery/">jQuery</a>
					</li>
					<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-568">
						<a href="http://www.anerbarrena.com/programacion/recursos/wordpress/">WordPress</a>
					</li>
					
					<li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent menu-item-has-children menu-item-46">
						<a href="http://www.anerbarrena.com/programacion/recursos/">Recursos</a>
						<ul class="sub-menu">
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-43">
								<a href="http://www.anerbarrena.com/programacion/htaccess-apache/">.htaccess</a>
							</li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1252">
								<a href="http://www.anerbarrena.com/programacion/recursos/google/">Google</a>
							</li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-299">
								<a href="http://www.anerbarrena.com/programacion/recursos/facebook-recursos/">Facebook</a>
							</li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2054">
								<a href="http://www.anerbarrena.com/programacion/recursos/twitter/">Twitter</a>
							</li>
							<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-378">
								<a href="http://www.anerbarrena.com/programacion/recursos/seo/">SEO</a>
							</li>
						</ul>
					</li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2914">
						<a href="http://www.anerbarrena.com/contactar/">Contactar</a>
					</li>
				</ul>
				<select class="responsiveMenuSelect">
					<option value="" selected="selected">=> Menú</option>
					<option  id="menu-item-41" value="http://www.anerbarrena.com/programacion/php/">PHP y MySQL</option>
					<option  id="menu-item-45" value="http://www.anerbarrena.com/programacion/php/mysql/">– MySQL</option>
					<option  id="menu-item-2250" value="http://www.anerbarrena.com/programacion/php/cadenas-string/">– Funciones y operaciones con cadenas string en PHP</option>
					<option  id="menu-item-1093" value="http://www.anerbarrena.com/programacion/php/fechas/">– Funciones y operaciones con fechas en PHP</option>
					<option  id="menu-item-42" value="http://www.anerbarrena.com/programacion/html5/">HTML5</option>
					<option  id="menu-item-1125" value="http://www.anerbarrena.com/programacion/html5/nuevas-etiquetas-y-atributos/">– Nuevos input, etiquetas y atributos en HTML5</option>
					<option  id="menu-item-2398" value="http://www.anerbarrena.com/programacion/html5/device-api-dispositivo/">– HTML5 Api</option>
					<option  id="menu-item-1001" value="http://www.anerbarrena.com/programacion/html5/form-html5/">– Formularios en HTML5</option>
					<option  id="menu-item-44" value="http://www.anerbarrena.com/programacion/jquery/">jQuery</option>
					<option  id="menu-item-568" value="http://www.anerbarrena.com/programacion/recursos/wordpress/">WordPress</option>
					<option  id="menu-item-46" value="http://www.anerbarrena.com/programacion/recursos/">Recursos</option>
					<option  id="menu-item-43" value="http://www.anerbarrena.com/programacion/htaccess-apache/">–.htaccess</option>
					<option  id="menu-item-1252" value="http://www.anerbarrena.com/programacion/recursos/google/">– Google</option>
					<option  id="menu-item-299" value="http://www.anerbarrena.com/programacion/recursos/facebook-recursos/">– Facebook</option>
					<option  id="menu-item-2054" value="http://www.anerbarrena.com/programacion/recursos/twitter/">– Twitter</option>
					
					<option  id="menu-item-378" value="http://www.anerbarrena.com/programacion/recursos/seo/">– SEO</option>
					<option  id="menu-item-2914" value="http://www.anerbarrena.com/contactar/">Contactar</option>
				</select>
			</nav>			

			 

<span style="margin:0 0 0 10px;"><img src="/img/publicidad.png"/></span>
<div style="text-align:center; margin-bottom:5px;">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-5475373752870298";
/* 728x90 skyscraper horizontal */
google_ad_slot = "6294765257";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
			<div id="main">
				<div id="content">
					<div class="hfeed">
						<article class="hentry post publish post-1 odd author-aner-barrena format-standard">	

	<h1>Ejemplo 1 de jQuery load(), pasando como par�metro una url</h1>
	<script src="https://apis.google.com/js/platform.js"></script>

<div style="display:inline; width:205px;">
	<span style="float:left;">
		<div class="g-ytsubscribe" data-channelid="UCcW1UbJOtTeESOZtpCVoc2g" data-layout="default" data-count="yes"></div>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<g:plusone size="medium" href="http://www.anerbarrena.com/demos/2013/03-jquery-load.php"></g:plusone>
		
	</span>
	<span style="bottom:-3px; position:relative;">
		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.anerbarrena.com/demos/2013/03-jquery-load.php&amp;width=190	&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=270775685307" scrolling	="no" frameborder="0" style="border:none; overflow:hidden; width:190px; height:21px;" allowTransparency="true"></iframe>
	
		<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.	html?via=anerbarrena&lang=es" style="width:110px; height:20px;"></iframe>
	
		<iframe allowtransparency="true" frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/follow_button.	html?screen_name=anerbarrena&lang=es&show_count=false" style="width:160px; height:20px;"></iframe>
	</span>
</div>								
	<p>
	Primer ejemplo de la funci�n <strong>jQuery load()</strong>, probamos la carga de un archivo externo pasando como par�metro la url.
	</p>
	<p>
	Esta demo est� documentada y explicada en el post original "<a href="http://www.anerbarrena.com/jquery-load-html-div-460/">jQuery load(): cargar un html externo en un div</a>".
	</p>
	<p>
	<h2>Ejemplo 1</h2>
	<div id="capa">Pulsa 'Actualizar capa' y este texto se actualizar�</div>
	<br>
   	<input name="boton" id="boton" type="button" value="Actualizar capa" />
	</p>

							</article>
						
							

<section id="comments">

	<div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'anerbarrena'; // required: replace example with your forum shortname

        
        var disqus_url = 'http://www.anerbarrena.com/jquery-load-html-div-460/';
        	
    	
        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    
</section><!-- #comments -->

						
					</div>
				</div>
			</div><!-- #main -->
		</div><!-- .wrap -->

				
		<aside id="sidebar-primary" class="sidebar">
			<section id="text-8" class="widget widget_text widget-widget_text"><h3 class="widget-title">S�gueme en:</h3>			
				<div class="textwidget">
					<div style="padding: 0 0 0 13px;">
						<a target="_blank" title="RSS anerbarrena.com" href="http://www.anerbarrena.com/feed/"><img style="padding:0; border:none;" alt="RSS anerbarrena.com" src="/img/RSS_48px.png"></a>
						<a target="_blank" title="Facebook anerbarrena.com" href="https://www.facebook.com/Anerbarrenaweb"><img style="padding:0; border:none;" alt="Facebook anerbarrena.com" src="/img/Facebook_48px.png"></a>
						<a target="_blank" title="Twitter anerbarrena.com" href="https://twitter.com/anerbarrena"><img style="padding:0; border:none;" alt="Twitter Aner Barrena" src="/img/Twitter_48px.png"></a>
						<a rel="author" title="Google Plus anerbarrena.com" target="_blank" href="https://plus.google.com/100521238980519601840"><img style="padding:0; border:none;" alt="Google + anerbarrena.com" src="/img/Google_48px.png"></a>
						<!--
						<a  target="_blank"  title="Canal Youtube anerbarrena.com" href="http://www.youtube.com/user/anerbarrena"><img style="padding:0; border:none;" alt="Youtube anerbarrena.com" src="/img/Youtube_48px.png"/></a>-->
					</div>
				</div>
			</section>
			
			<section id="text-8" class="widget widget_text widget-widget_text">	
				<span  style="margin:0 0 0 0;"><img src="/img/publicidad.png"/></span>
				<div style="position:relative;left:-10px;">
					<script type="text/javascript"><!--
					google_ad_client = "ca-pub-5475373752870298";
					/* robapaginas */
					google_ad_slot = "4445721252";
					google_ad_width = 300;
					google_ad_height = 250;
					//-->
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</div>
			</section>

			<section id="hybrid-search-2" class="widget search widget-search">
				<h3 class="widget-title">Buscador</h3>
				<form method="get" class="search-form" id="search-formhybrid-search" action="http://www.anerbarrena.com/">
					<div>
						<input class="search-text" name="s" id="search-texthybrid-search" value="Busca y encontrar�s..." onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" type="text">
					</div>
				</form>
			</section>
			<section id="text-3" class="widget widget_text widget-widget_text">			
				<div class="textwidget"><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fanerbarrenaweb&amp;width=280&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=270775685307" style="border:none; overflow:hidden; width:280px; height:258px;" allowtransparency="true" frameborder="0" scrolling="no"></iframe></div>
			</section>

			<section id="text-8" class="widget widget_text widget-widget_text">	
				<span  style="margin:0 0 0 0;"><img src="/img/publicidad.png"/></span>

				<div style="position:relative;left:-10px;margin: 0 0 5px 0;">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-5475373752870298";
				/* robapaginas doble */
				google_ad_slot = "6746381652";
				google_ad_width = 300;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
				</div>
			</section>

			<section id="text-6" class="widget widget_text widget-widget_text">
					<h3 class="widget-title">Top post</h3>			
					<div class="textwidget">
						<ul>
<li><a href="http://www.anerbarrena.com/jquery-load-html-div-460/" title="jQuery load(): cargar un html externo en un div">jQuery load(): cargar un html externo en un div</a></li>
<li><a href="http://www.anerbarrena.com/jquery-toogle-mostrar-ocultar-divs-1728/" title="jQuery toggle(): Mostrar, ocultar y animar elementos en tu web">jQuery toggle(): Mostrar, ocultar y animar elementos en tu web</a></li>
<li><a href="http://www.anerbarrena.com/value-radio-button-jquery-checked-1580/" title="Obtener el valor de un radio button con jQuery, comprobar si est� seleccionado y checkearlo">Obtener el valor de un radio button con jQuery, comprobar si est� seleccionado y checkearlo</a></li>
<li><a href="http://www.anerbarrena.com/jquery-change-1879/" title="jQuery change(): Detectar cambios de valor en los elementos de tu web">jQuery change(): Detectar cambios de valor en los elementos de tu web</a></li>
<li><a href="http://www.anerbarrena.com/php-date-1018/" title="PHP date(): Obtener d�a, mes, a�o y hora actual con PHP">PHP date(): Obtener d�a, mes, a�o y hora actual con PHP</a></li>
<li><a href="http://www.anerbarrena.com/date-input-html5-2829/" title="Date input HTML5: Un sencillo datepicker para tu formulario">Date input HTML5: Un sencillo datepicker para tu formulario</a></li>
<li>
<a title="ejemplos de arrays en PHP" href="http://www.anerbarrena.com/php-array-tipos-ejemplos-3876/">PHP array(): tipos de matrices y ejemplos</a>
</li>
</u>
					</div>
			</section>
			<section id="hybrid-bookmarks-2" class="linkcat widget bookmarks widget-bookmarks">
				<h3 class="widget-title">Webs amigas</h3>
				<ul class="xoxo blogroll">
					<li><a href="http://www.egocast.es/" rel="met co-worker colleague" title="Viajes &amp; Internet" target="_blank">Egocast</a></li>
					<li>
					<a href="http://www.menecesitas.com" rel="met co-worker colleague" title="Lesiones deportivas y dietas para adelgazar para mujeres deportistas" target="_blank">Lesiones deportivas y dietas para adelgazar</a></li>
				</ul>
			</section>
			<section id="text-2" class="widget widget_text widget-widget_text">			
				<div data-twttr-id="twttr-sandbox-0" class="textwidget">
					<a class="twitter-timeline"  href="https://twitter.com/anerbarrena"  data-widget-id="346901740383256578">Tweets por @anerbarrena</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</section>
			
		</aside>
	
		<footer id="footer">
			<div class="footer-content">
				<p class="credit">Copyright &#64; 2013 <a class="site-link" href="http://www.anerbarrena.com" title="Aner Barrena" rel="home"><span>Aner Barrena</span></a>. Powered by <a class="wp-link" href="http://wordpress.org" title="State-of-the-art semantic personal publishing platform"><span>WordPress</span></a> and <a class="theme-link" href="http://wordpress.org/themes/chun" title=" WordPress Theme"><span>Chun theme</span></a> | Contactar: <a class="wp-link" href="http://www.anerbarrena.com/contactar/"><span>Contactar</span></a>
					|  <a class="wp-link" href="http://www.anerbarrena.com/politica-de-cookies/"><span>Pol&iacute;tica de  cookies</span></a>
				</p>			
			</div>
		</footer>
	</div><!-- #container -->
	
	<!-- Google plus boton -->
	<script type="text/javascript">
	  window.___gcfg = {lang: 'es'};
	
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script></body>
</html>